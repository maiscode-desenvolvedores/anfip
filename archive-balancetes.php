<?php get_header(); 

$url_redirect = get_home_url();

if (!is_user_logged_in()) {
	wp_redirect($url_redirect);
}
?>
<section id="sec_top_patrimoniais" style="background-image: url(<?php echo get_option('banner_balancetes'); ?>);">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<h1><?php echo get_option('titulo_balancetes'); ?></h1>
			</div>
		</div>
	</div>
</section>
<section id="sec_patrimoniais_patrimoniais">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<ul class="box_list_post box_list_post_balancetes my-posts">
				<?php if (have_posts()) : while(have_posts()) : the_post(); ?>
                        <h2><span><?php the_title(); ?></span></h2>	<?php if( have_rows('arquivos') ):  while ( have_rows('arquivos') ) : the_row(); ?>
						<li>  
                            <p><?php the_sub_field('titulo_pdf_balancete'); ?></p>
                            <a href="<?php the_sub_field('pdf_balancete'); ?>" target="_blank">
                                <button><?php echo get_option('texto_btn_balancetes'); ?></button>
                            </a>
                        </li>
                        <hr>
                        <?php endwhile; endif; ?>
						
					<?php endwhile ?>
				<?php else : ?>
					<h2>Nada Encontrado</h2>
				<?php endif; ?>	
				</ul>
			</div>
		</div>
		<!-- Carrega Mais -->
        <?php get_template_part('inc/load', 'more'); ?>	
	</div>	
</section>

<?php get_footer(); 