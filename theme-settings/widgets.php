<?php
/**
*Author: Mais Code Tecnologia - Equipe Gestão Ativa
*Tema: Modelo Padrao - WordPress Theme
*Produção: Mais Code - Revisão Rodolfo
*Arquivo: Arquivo de Redes Sociais
*@link http://www.maiscode.com.br
*@package WordPress Communicate English Theme
*@since: 26/02/2020
*/      

class UltimosPost extends WP_Widget {

    function __construct() {
        parent::__construct(
            'ultimosPost_site', // Base ID
            esc_html__( 'Ultimos Post', 'text_domain' ), // Name
            array( 'description' => esc_html__( 'Bloco da Últimos Posts', 'text_domain' ), ) // Args
        );
    }

    public function form( $instance ) {
        $title = ! empty( $instance['title'] ) ? $instance['title'] : esc_html__( 'New title', 'text_domain' );
        ?>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>">limite de post</label> 
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
        </p>
    <?php 
    }

    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        return $instance;
    }

    public function widget( $args, $instance ) {
        echo $args['before_widget']; ?>
        <div class="col-12 redeSocialSite px-0">
            <div class="social">
                <?php 
                    // if ( ! empty( $instance['title'] ) ) {
                    //     echo apply_filters( 'widget_texto', $instance['title'] );
                    // }

                    $args_post = array(
                        'post_type' => 'post',
                        'posts_per_page' => 2
                    );

                    $all_posts = new WP_Query($args_post);
                    
                    if ($all_posts->have_posts()) :
                        $cont = 1;
                        ?>
                        <h3>Últimos posts</h3>
                        <?php 
                        while ($all_posts->have_posts()) : $all_posts->the_post();
                            $image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()));
                            $tipo = get_field('tipo_de_noticia',get_the_ID());
                            //var_dump($tipo);
                            ?>
                            <a href="<?php the_permalink(); ?>">
                            <div class="box_post_footer d-flex">
                                <?php the_post_thumbnail('thumbnail'); ?>
                                <div class="box_cont_post_footer">
                                    <h2><?php the_title(); ?></h2>   
                                    <div class="d-flex">
                                        <?php if ($tipo == 'Notícia padrão'): ?>

                                            <?php if( get_field('local_noticia') ): ?>
                                                    <p><?php the_field('local_noticia'); ?></p>
                                                        <p class="p-10">|</p>
                                            <?php endif; ?>                            
   
                                             <p><?php the_time( 'j  M  Y' ); ?></p>          

                                          <?php else: ?>

                                           <?php if( get_field('local_noticia_especial') ): ?>
                                                    <p><?php the_field('local_noticia_especial'); ?></p>
                                                        <p class="p-10">|</p>
                                            <?php endif; ?>     
                         
                                            <p><?php the_time( 'j  M  Y' ); ?></p>
                                        <?php endif ?>
                                    </div>
                                </div>
                            </div>
                            </a> 
                            <?php if($cont < 3): ?>
                                <hr class="hr_post_footer">    
                            <?php endif ?>
                            
                            <?php 
                            $cont++;       
                        endwhile;
                    endif;
                    wp_reset_postdata();
                ?>
            </div>
        </div>
        <?php           
        echo $args['after_widget'];
    }

   
}

function register_foo_widget_ultimosPosts() {
    register_widget( 'UltimosPost' );
}
add_action( 'widgets_init', 'register_foo_widget_ultimosPosts' ); 

function my_custom_sidebar() {
    register_sidebar(
        array (
            'name' => __( 'Sidebar footer', 'your-theme-domain' ),
            'id' => 'custom-side-bar-footer',
            'description' => __( 'Sidebar footer', 'your-theme-domain' ),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
}
add_action( 'widgets_init', 'my_custom_sidebar' );






class UltimasNoticias extends WP_Widget {

    function __construct() {
        parent::__construct(
            'ultimasNoticias_site', // Base ID
            esc_html__( 'Últimas Notícias', 'text_domain' ), // Name
            array( 'description' => esc_html__( 'Bloco da Últimas Notícias', 'text_domain' ), ) // Args
        );
    }

    public function form( $instance ) {
        $title = ! empty( $instance['title'] ) ? $instance['title'] : esc_html__( 'New title', 'text_domain' );
        ?>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>">limite de post</label> 
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
        </p>
    <?php 
    }

    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';


        return $instance;
    }

    public function widget( $args, $instance ) {
        echo $args['before_widget']; ?>
        <div class="redeSocialSite">
            <div class="box_sidebar_noticia">
                <?php 
                    // if ( ! empty( $instance['title'] ) ) {
                    //     echo apply_filters( 'widget_texto', $instance['title'] );
                    // }

                    $args_post = array(
                        'post_type' => 'post',
                        'posts_per_page' => 4
                    );

                    $all_posts = new WP_Query($args_post);
                    
                    if ($all_posts->have_posts()) :
                        $cont = 1;
                        ?>
                        <h3><div></div>ÚLTIMAS NOTÍCIAS</h3>
                        <?php
                        $destaque = 'destaque'; 
                        while ($all_posts->have_posts()) : $all_posts->the_post();
                            $image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()));
                            ?>
                            <div class="box_noticia_sidebar">
                                <?php if ($destaque == 'destaque'): ?>
                                    <img src="<?php echo $image; ?>">
                                    <a href="<?php the_permalink(); ?>">
                                        <h4><?php the_title(); ?></h4>
                                    </a>
                                    <ul>
                                        <li><?php the_field('data_noticia') ?></li>
                                        <!-- <li> | </li> -->
                                        <li> <?php echo gt_get_post_view(); ?></li>
                                    </ul>
                                    <p><?php the_field('breve_resumo'); ?></p>
                                <?php
                                $destaque = '';
                                else:?>
                                    <a href="<?php the_permalink(); ?>">
                                        <h4><?php the_title(); ?></h4>
                                    </a>
                                    <ul>
                                        <li><?php the_field('data_noticia') ?></li>
                                        <!-- <li> | </li> -->
                                        <li><?php echo gt_get_post_view(); ?></li>
                                    </ul>
                                <?php endif ?>
                            </div> 
                            <?php 
                            $cont++;       
                        endwhile;
                    endif;
                    wp_reset_postdata();
                ?>
            </div>
        </div>
        <?php           
        echo $args['after_widget'];
    }

   
}

function register_foo_widget_ultimasnoticias() {
    register_widget( 'UltimasNoticias' );
}
add_action( 'widgets_init', 'register_foo_widget_ultimasnoticias' );





function my_custom_sidebar_footer_2() {
    register_sidebar(
        array (
            'name' => __( 'Sidebar footer 2', 'your-theme-domain' ),
            'id' => 'custom-side-bar-footer-2',
            'description' => __( 'Sidebar footer', 'your-theme-domain' ),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
}
add_action( 'widgets_init', 'my_custom_sidebar_footer_2' );


class servicos extends WP_Widget {

    function __construct() {
        parent::__construct(
            'servicos_site', // Base ID
            esc_html__( 'Serviços', 'text_domain' ), // Name
            array( 'description' => esc_html__( 'Bloco da Últimas Notícias', 'text_domain' ), ) // Args
        );
    }

    public function form( $instance ) {
        $title = ! empty( $instance['title'] ) ? $instance['title'] : esc_html__( 'New title', 'text_domain' );
        ?>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>">limite de post</label> 
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
        </p>
    <?php 
    }

    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';


        return $instance;
    }

    public function widget( $args, $instance ) {
        echo $args['before_widget']; ?>
        <div class="redeSocialSite">
            <div class="row">
                <?php $widget_id = 'custom-side-bar-footer-2';
                if (have_rows('servicos')): ?>
                    <?php while(have_rows('servicos')): the_row(); ?>
                        <div class="col-12 col-md-6">
                            <a href="<?php the_sub_field('link_servico','widget_' . $widget_id) ?>"><?php the_sub_field('titulo_servico','widget_' . $widget_id); ?></a>
                        </div>
                    <?php endwhile ?>
                <?php endif ?>
            </div>
        </div>
        <?php           
        echo $args['after_widget'];
    }
}

function register_foo_widget_servicos() {
    register_widget( 'servicos' );
}
add_action( 'widgets_init', 'register_foo_widget_servicos' );




class Anuncio extends WP_Widget {

    function __construct() {
        parent::__construct(
            'Anuncio_site', // Base ID
            esc_html__( 'Serviços', 'text_domain' ), // Name
            array( 'description' => esc_html__( 'Bloco da Últimas Notícias', 'text_domain' ), ) // Args
        );
    }

    public function form( $instance ) {
        $title = ! empty( $instance['title'] ) ? $instance['title'] : esc_html__( 'New title', 'text_domain' );
        ?>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>">limite de post</label> 
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
        </p>
    <?php 
    }

    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';


        return $instance;
    }

    public function widget( $args, $instance ) {
        echo $args['before_widget']; ?>
        <div class="redeSocialSite">
            <div class="row">
                <?php $widget_id = 'custom-side-bar-footer-2';
                if (have_rows('Anuncio')): ?>
                    <?php while(have_rows('Anuncio')): the_row(); ?>
                        <div class="col-12 col-md-6">
                            <a href="<?php the_sub_field('link_servico','widget_' . $widget_id) ?>"><?php the_sub_field('titulo_servico','widget_' . $widget_id); ?></a>
                        </div>
                    <?php endwhile ?>
                <?php endif ?>
            </div>
        </div>
        <?php           
        echo $args['after_widget'];
    }
}

function register_foo_widget_Anuncio() {
    register_widget( 'Anuncio' );
}
add_action( 'widgets_init', 'register_foo_widget_Anuncio' );