<?php
/**
*Author: Mais Code Tecnologia - Equipe Gestão Ativa
*Tema: Modelo Padrao - WordPress Theme
*Produção: Mais Code - Revisão Rodolfo
*Arquivo: Arquivo de Redes Sociais
*@link http://www.maiscode.com.br
*@package WordPress Communicate English Theme
*@since: 23/04/2018
*/

add_action('admin_menu', 'create_menu');
add_action('widgets_init', create_function('', 'return register_widget("social_widget");'));


function create_menu() {		

	add_menu_page(
		'Dados Site', 
		'Dados Site', 
		'manage_options', 
		'espaco-contato', 
		'settingsSobre_page', 
		'dashicons-share'
	);

	add_submenu_page( 
		'espaco-contato', 
		'Newsletter', 
		'Newsletter', 
		'manage_options',
		'newsletter', 
		'settingsNewsletter'
	);

	add_submenu_page( 
		'espaco-contato', 
		'Redes Sociais', 
		'Redes Sociais', 
		'manage_options',
		'redes-socias', 
		'settingsSocial_page'
	);

	add_submenu_page(
		'options-general.php',
		'Tag Manager',
		'Tag Manager',
		'manage_options',
		'tag-manager',
		'wp_tag_manager_submenu_page' );

	add_submenu_page(
		'edit.php?post_type=fotos',
		'Dados Fotos',
		'Dados Fotos',
		'manage_options',
		'fotos-manager',
		'wp_fotospage' );

	add_submenu_page(
		'edit.php',
		'Dados Notícias',
		'Dados Notícias',
		'manage_options',
		'noticias-manager',
		'wp_noticiaspage' );

	add_submenu_page(
		'edit.php?post_type=artigos',
		'Dados artigos',
		'Dados artigos',
		'manage_options',
		'artigos-manager',
		'wp_artigospage' );

	add_submenu_page(
		'edit.php?post_type=publicacoes',
		'Dados publicacoes',
		'Dados publicacoes',
		'manage_options',
		'publicacoes-manager',
		'wp_publicacoespage' );

	add_submenu_page(
		'edit.php?post_type=videos',
		'Dados videos',
		'Dados videos',
		'manage_options',
		'videos-manager',
		'wp_videospage' );

	add_submenu_page(
		'edit.php?post_type=literarios',
		'Dados literarios',
		'Dados literarios',
		'manage_options',
		'literarios-manager',
		'wp_literariospage' );

	add_submenu_page(
		'edit.php?post_type=patrimoniais',
		'Dados patrimoniais',
		'Dados patrimoniais',
		'manage_options',
		'patrimoniais-manager',
		'wp_patrimoniaispage' );
	add_submenu_page(
		'edit.php?post_type=relatorios_atividade',
		'Dados Relatorios de Atividades',
		'Dados Relatorios de Atividades',
		'manage_options',
		'relatorios_atividade-manager',
		'wp_relatorios_atividadepage' );
	add_submenu_page(
		'edit.php?post_type=colegas',
		'Dados colegas',
		'Dados colegas',
		'manage_options',
		'colegas-manager',
		'wp_colegaspage' );
	add_submenu_page(
		'edit.php?post_type=expresidentes',
		'Dados Ex presidentes',
		'Dados Ex presidentes',
		'manage_options',
		'expresidente-manager',
		'wp_expresidentespage' );
	add_submenu_page(
		'edit.php?post_type=eventos',
		'Dados eventos',
		'Dados eventos',
		'manage_options',
		'eventos-manager',
		'wp_eventospage' );
	add_submenu_page(
		'edit.php?post_type=conselho',
		'Dados conselho',
		'Dados conselho',
		'manage_options',
		'conselho-manager',
		'wp_conselhopage' );
	add_submenu_page(
		'edit.php?post_type=convenio',
		'Dados convenio',
		'Dados convenio',
		'manage_options',
		'convenio-manager',
		'wp_conveniopage' );
	add_submenu_page(
		'edit.php?post_type=balancetes',
		'Dados Balancetes',
		'Dados Balancetes',
		'manage_options',
		'balancetes-manager',
		'wp_balancetepage' );
	
		//call register settings function
	add_action( 'admin_init', 'register_mysettings' );
	add_action( 'admin_init', 'register_mysettingsSobre' );
	add_action( 'admin_init', 'register_mysettingsNewsletter' );
	add_action( 'admin_init', 'register_mysettingsTag' );
	add_action( 'admin_init', 'register_mysettingsFotos' );
	add_action( 'admin_init', 'register_mysettingsArtigos' );
	add_action( 'admin_init', 'register_mysettingsPublicacoes' );
	add_action( 'admin_init', 'register_mysettingsVideos' );
	add_action( 'admin_init', 'register_mysettingsLiterarios' );
	add_action( 'admin_init', 'register_mysettingsPatrimoniais' );
	add_action( 'admin_init', 'register_mysettingsRelatorios' );
	add_action( 'admin_init', 'register_mysettingsColegas' );
	add_action( 'admin_init', 'register_mysettingsExpresidentes' );
	add_action( 'admin_init', 'register_mysettingsEventos' );
	add_action( 'admin_init', 'register_mysettingsConselho' );
	add_action( 'admin_init', 'register_mysettingsNoticias' );
	add_action( 'admin_init', 'register_mysettingsConvenio' );
	add_action( 'admin_init', 'register_mysettingsBalancetes' );
}


function register_mysettings() {
	register_setting( 'settings-group', 'facebook' );
	register_setting( 'settings-group', 'twitter' );
	register_setting( 'settings-group', 'google' );
	register_setting( 'settings-group', 'youtube' );
	register_setting( 'settings-group', 'instagram' );
	register_setting( 'settings-group', 'linkedin' );
	register_setting( 'settings-group', 'texto_redes_sociais' );	
}

function register_mysettingsNewsletter() {
	register_setting( 'settings-group-newsletter', 'texto_form_inscrever' );
	register_setting( 'settings-group-newsletter', 'imagem_form_inscrever' );
}

function register_mysettingsSobre() {
	register_setting( 'settings-group-sobre', 'email' );
	register_setting( 'settings-group-sobre', 'telefone' );
	register_setting( 'settings-group-sobre', 'endereco' );
	register_setting( 'settings-group-sobre', 'banner_contato' );
	register_setting( 'settings-group-sobre', 'titulo_contato' );
	register_setting( 'settings-group-sobre', 'sub_titulo_contato' );
	register_setting( 'settings-group-sobre', 'header_logo' );
	register_setting( 'settings-group-sobre', 'titulo_anfip_footer' );
	register_setting( 'settings-group-sobre', 'texto_anfip_footer' );
	register_setting( 'settings-group-sobre', 'texto_rodape' );
	register_setting( 'settings-group-sobre', 'texto_form_recupera_senha' );
	register_setting( 'settings-group-sobre', 'titulo_form_recupera_senha' );
	register_setting( 'settings-group-sobre', 'texto_form_cadastro' );
	register_setting( 'settings-group-sobre', 'titulo_form_cadastro' );
}
function register_mysettingsTag() {
	register_setting( 'settings-group-tag', 'tag_head' );
	register_setting( 'settings-group-tag', 'tag_body' );
	register_setting( 'settings-group-tag', 'api_maps' );
}


function register_mysettingsFotos() {	
	register_setting( 'settings-group-fotos', 'titulo_fotos' );
	//register_setting( 'settings-group-fotos', 'subtitulo_fotos' );
	register_setting( 'settings-group-fotos', 'banner_fotos' );
	//register_setting( 'settings-group-fotos', 'texto_botao_fotos' );
	register_setting( 'settings-group-fotos', 'post_per_page_fotos' );
}

function register_mysettingsArtigos() {	
	register_setting( 'settings-group-artigos', 'titulo_artigos' );
	//register_setting( 'settings-group-artigos', 'subtitulo_artigos' );
	register_setting( 'settings-group-artigos', 'banner_artigos' );
	register_setting( 'settings-group-artigos', 'texto_btn_artigo' );
	register_setting( 'settings-group-artigos', 'post_per_page_artigos' );
}

function register_mysettingsPublicacoes() {	
	register_setting( 'settings-group-publicacoes', 'titulo_publicacoes' );
	//register_setting( 'settings-group-publicacoes', 'subtitulo_publicacoes' );
	register_setting( 'settings-group-publicacoes', 'banner_publicacoes' );
	register_setting( 'settings-group-publicacoes', 'texto_btn_publicacao' );
	register_setting( 'settings-group-publicacoes', 'txt_btn_pub_download' );
	register_setting( 'settings-group-publicacoes', 'banner_home' );
	register_setting( 'settings-group-publicacoes', 'icone_banner_home' );
	register_setting( 'settings-group-publicacoes', 'post_per_page_publicacoes' );
}

function register_mysettingsVideos() {	
	register_setting( 'settings-group-videos', 'titulo_videos' );
	//register_setting( 'settings-group-videos', 'subtitulo_videos' );
	register_setting( 'settings-group-videos', 'banner_videos' );
	register_setting( 'settings-group-videos', 'texto_btn_videos' );
	register_setting( 'settings-group-videos', 'post_per_page_videos' );
}
function register_mysettingsLiterarios() {	
	register_setting( 'settings-group-literarios', 'titulo_literarios' );
	//register_setting( 'settings-group-literarios', 'subtitulo_literarios' );
	register_setting( 'settings-group-literarios', 'banner_literarios' );
	register_setting( 'settings-group-literarios', 'texto_btn_literarios' );
	register_setting( 'settings-group-literarios', 'post_per_page_literarios' );
}
function register_mysettingsPatrimoniais() {	
	register_setting( 'settings-group-patrimoniais', 'titulo_patrimoniais' );
	//register_setting( 'settings-group-patrimoniais', 'subtitulo_patrimoniais' );
	register_setting( 'settings-group-patrimoniais', 'banner_patrimoniais' );
	register_setting( 'settings-group-patrimoniais', 'texto_btn_patrimoniais' );
	register_setting( 'settings-group-patrimoniais', 'post_per_page_patrimoniais' );
}
function register_mysettingsRelatorios() {	
	register_setting( 'settings-group-relatorios_atividade', 'titulo_relatorios_atividade' );
	//register_setting( 'settings-group-relatorios_atividade', 'subtitulo_relatorios_atividade' );
	register_setting( 'settings-group-relatorios_atividade', 'banner_relatorios_atividade' );
	register_setting( 'settings-group-relatorios_atividade', 'texto_btn_relatorios_atividade' );
	register_setting( 'settings-group-relatorios_atividade', 'post_per_page_relatorios_atividade' );
}

function register_mysettingsColegas() {	
	register_setting( 'settings-group-colegas', 'titulo_colegas' );
	//register_setting( 'settings-group-colegas', 'subtitulo_colegas' );
	register_setting( 'settings-group-colegas', 'banner_colegas' );
	register_setting( 'settings-group-colegas', 'post_per_page_colegas' );
}
function register_mysettingsExpresidentes() {	
	register_setting( 'settings-group-expresidentes', 'titulo_expresidentes' );
	//register_setting( 'settings-group-expresidentes', 'subtitulo_expresidentes' );
	register_setting( 'settings-group-expresidentes', 'banner_expresidentes' );
	register_setting( 'settings-group-expresidentes', 'titulo_nomes' );
	register_setting( 'settings-group-expresidentes', 'titulo_mandatos' );
	register_setting( 'settings-group-expresidentes', 'post_per_page_expresidentes' );
}
function register_mysettingsEventos() {	
	register_setting( 'settings-group-eventos', 'titulo_eventos' );
	//register_setting( 'settings-group-eventos', 'subtitulo_eventos' );
	register_setting( 'settings-group-eventos', 'banner_eventos' );
	register_setting( 'settings-group-eventos', 'post_per_page_eventos' );
}

function register_mysettingsConselho() {	
	register_setting( 'settings-group-conselho', 'titulo_conselho' );
	//register_setting( 'settings-group-conselho', 'subtitulo_conselho' );
	register_setting( 'settings-group-conselho', 'banner_conselho' );
	register_setting( 'settings-group-conselho', 'titulo_executivo' );
	register_setting( 'settings-group-conselho', 'titulo_fiscal' );
	register_setting( 'settings-group-conselho', 'post_per_page_conselho' );
}

function register_mysettingsNoticias() {	
	register_setting( 'settings-group-noticias', 'titulo_primeiro_noticias' );
	register_setting( 'settings-group-noticias', 'titulo_segundo_noticias' );
}

function register_mysettingsConvenio() {	
	register_setting( 'settings-group-convenio', 'titulo_convenio_topo' );
	register_setting( 'settings-group-convenio', 'subtitulo_convenio' );
	register_setting( 'settings-group-convenio', 'banner_convenio' );
	register_setting( 'settings-group-convenio', 'titulo_convenios' );
	register_setting( 'settings-group-convenio', 'titulo_associados' );
	register_setting( 'settings-group-convenio', 'banner_associado' );
	register_setting( 'settings-group-convenio', 'texto_associados' );
	register_setting( 'settings-group-convenio', 'texto_btn_associado' );
}
function register_mysettingsBalancetes() {	
	register_setting( 'settings-group-balancetes', 'titulo_balancetes' );
	register_setting( 'settings-group-balancetes', 'banner_balancetes' );
	register_setting( 'settings-group-balancetes', 'texto_btn_balancetes' );
	register_setting( 'settings-group-balancetes', 'post_per_page_balancetes' );
}
function mypage() { ?>

<?php }
// Setter
function wp_conveniopage(){ ?>
	<div class="wrap">
		<div id="icon-tools" class="icon32"></div>
		<form method="post" action="options.php" style="float:left; width:70%">
			<?php settings_fields( 'settings-group-convenio' ); ?>
			<fieldset style=" width:100%; padding:20px; margin:30px;">
				<h2 style="font-weight:bold; font-size:24px;">Dados Convênios e Benefícios</h2>
				<?php if (isset($_GET['settings-updated'])) : ?>
					<div class="notice notice-success is-dismissible"><p>Atualizado com sucesso.</p></div>
				<?php endif; ?>
				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Titulo</label>
					<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="titulo_convenio_topo" type="text" value="<?php echo get_option('titulo_convenio_topo'); ?>" />
					<p style="color: #999999;font-size: 13px;margin: 5px 0 0 5px">Ex: Eternos convenio</p>
				</div>
				
				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Sub Titulo</label>
					<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="subtitulo_convenio" type="text" value="<?php echo get_option('subtitulo_convenio'); ?>" />
					<p style="color: #999999;font-size: 13px;margin: 5px 0 0 5px">Ex: Eternos convenio</p>
				</div>

				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Banner</label>
					<?php 
					if(function_exists( 'wp_enqueue_media' )){
						wp_enqueue_media();
					}else{
						wp_enqueue_style('thickbox');
						wp_enqueue_script('media-upload');
						wp_enqueue_script('thickbox');
					} ?>
					<img class="banner_convenio" src="<?php echo get_option('banner_convenio'); ?>" height="100" width="250" style="margin-bottom: 10px;"/><br>
					<input class="banner_convenio_url" type="hidden" name="banner_convenio" size="60" value="<?php echo get_option('banner_convenio'); ?>">
					<a href="#" class="page-title-action banner_convenio_upload">Upload</a>
					<script>
						jQuery(document).ready(function($) {
							$('.banner_convenio_upload').click(function(e) {
								e.preventDefault();
								var custom_uploader = wp.media({
									title: 'Custom Image',
									button: {
										text: 'Upload Image'
									},
	                                    multiple: false  // Set this to true to allow multiple files to be selected
	                                })
								.on('select', function() {
									var attachment = custom_uploader.state().get('selection').first().toJSON();
									$('.banner_convenio').attr('src', attachment.url);
									$('.banner_convenio_url').val(attachment.url);
								})
								.open();
							});
						});
					</script>
				</div>
				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Titulo convênios</label>
					<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="titulo_convenios" type="text" value="<?php echo get_option('titulo_convenios'); ?>" />
					<p style="color: #999999;font-size: 13px;margin: 5px 0 0 5px">Ex: Eternos convenio</p>
				</div>
				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Titulo associados</label>
					<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="titulo_associados" type="text" value="<?php echo get_option('titulo_associados'); ?>" />
					<p style="color: #999999;font-size: 13px;margin: 5px 0 0 5px">Ex: Eternos convenio</p>
				</div>
				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Banner</label>
					<?php 
					if(function_exists( 'wp_enqueue_media' )){
						wp_enqueue_media();
					}else{
						wp_enqueue_style('thickbox');
						wp_enqueue_script('media-upload');
						wp_enqueue_script('thickbox');
					} ?>
					<img class="banner_associado" src="<?php echo get_option('banner_associado'); ?>" height="100" width="250" style="margin-bottom: 10px;"/><br>
					<input class="banner_associado_url" type="hidden" name="banner_associado" size="60" value="<?php echo get_option('banner_associado'); ?>">
					<a href="#" class="page-title-action banner_associado_upload">Upload</a>
					<script>
						jQuery(document).ready(function($) {
							$('.banner_associado_upload').click(function(e) {
								e.preventDefault();
								var custom_uploader = wp.media({
									title: 'Custom Image',
									button: {
										text: 'Upload Image'
									},
	                                    multiple: false  // Set this to true to allow multiple files to be selected
	                                })
								.on('select', function() {
									var attachment = custom_uploader.state().get('selection').first().toJSON();
									$('.banner_associado').attr('src', attachment.url);
									$('.banner_associado_url').val(attachment.url);
								})
								.open();
							});
						});
					</script>
				</div>
				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Texto associados</label>
					
					<textarea style="width:100%; background:#fff; border-radius:5px; height:100px" name="texto_associados" type="text"><?php echo get_option('texto_associados'); ?></textarea>
					<p style="color: #999999;font-size: 13px;margin: 5px 0 0 5px">Ex: Eternos convenio</p>
				</div>
				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Texto botão associado</label>
					<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="texto_btn_associado" type="text" value="<?php echo get_option('texto_btn_associado'); ?>" />
					<p style="color: #999999;font-size: 13px;margin: 5px 0 0 5px">Ex: Eternos convenio</p>
				</div>
				<div style="clear:both"></div>
				<p class="submit">
					<input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />
				</p>
			</fieldset>		
		</form>
	</div>
<?php 
}
function wp_balancetepage(){ ?>
	<div class="wrap">
		<div id="icon-tools" class="icon32"></div>
		<form method="post" action="options.php" style="float:left; width:70%">
			<?php settings_fields( 'settings-group-balancetes' ); ?>
			<fieldset style=" width:100%; padding:20px; margin:30px;">
				<h2 style="font-weight:bold; font-size:24px;">Dados Balancetes</h2>
				<?php if (isset($_GET['settings-updated'])) : ?>
					<div class="notice notice-success is-dismissible"><p>Atualizado com sucesso.</p></div>
				<?php endif; ?>
				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Título Banner</label>
					<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="titulo_balancetes" type="text" value="<?php echo get_option('titulo_balancetes'); ?>" />
					<p style="color: #999999;font-size: 13px;margin: 5px 0 0 5px">Ex: Serviços</p>
				</div>
				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Texto Botão</label>
					<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="texto_btn_balancetes" type="text" value="<?php echo get_option('texto_btn_balancetes'); ?>" />
					<p style="color: #999999;font-size: 13px;margin: 5px 0 0 5px">Ex: Serviços</p>
				</div>
				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Exibir posts</label>
					<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="post_per_page_balancetes" type="text" value="<?php echo get_option('post_per_page_balancetes'); ?>" />
					<p style="color: #999999;font-size: 13px;margin: 5px 0 0 5px">Ex: 10</p>
				</div>
				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Banner</label>
					<?php 
					if(function_exists( 'wp_enqueue_media' )){
						wp_enqueue_media();
					}else{
						wp_enqueue_style('thickbox');
						wp_enqueue_script('media-upload');
						wp_enqueue_script('thickbox');
					} ?>
					<img class="banner_balancetes" src="<?php echo get_option('banner_balancetes'); ?>" height="100" width="250" style="margin-bottom: 10px;"/><br>
					<input class="banner_balancetes_url" type="hidden" name="banner_balancetes" size="60" value="<?php echo get_option('banner_balancetes'); ?>">
					<a href="#" class="page-title-action banner_balancetes_upload">Upload</a>
					<script>
						jQuery(document).ready(function($) {
							$('.banner_balancetes_upload').click(function(e) {
								e.preventDefault();
								var custom_uploader = wp.media({
									title: 'Custom Image',
									button: {
										text: 'Upload Image'
									},
	                                    multiple: false  // Set this to true to allow multiple files to be selected
	                                })
								.on('select', function() {
									var attachment = custom_uploader.state().get('selection').first().toJSON();
									$('.banner_balancetes').attr('src', attachment.url);
									$('.banner_balancetes_url').val(attachment.url);
								})
								.open();
							});
						});
					</script>
				</div>
				<div style="clear:both"></div>
				<p class="submit">
					<input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />
				</p>
			</fieldset>		
		</form>
	</div>
<?php 
}
function wp_relatorios_atividadepage(){ ?>
	<div class="wrap">
		<div id="icon-tools" class="icon32"></div>
		<form method="post" action="options.php" style="float:left; width:70%">
			<?php settings_fields( 'settings-group-relatorios_atividade' ); ?>
			<fieldset style=" width:100%; padding:20px; margin:30px;">
				<h2 style="font-weight:bold; font-size:24px;">Dados Relatórios de Atividades</h2>
				<?php if (isset($_GET['settings-updated'])) : ?>
					<div class="notice notice-success is-dismissible"><p>Atualizado com sucesso.</p></div>
				<?php endif; ?>
				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Título Banner</label>
					<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="titulo_relatorios_atividade" type="text" value="<?php echo get_option('titulo_relatorios_atividade'); ?>" />
					<p style="color: #999999;font-size: 13px;margin: 5px 0 0 5px">Ex: Serviços</p>
				</div>
				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Texto Botão</label>
					<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="texto_btn_relatorios_atividade" type="text" value="<?php echo get_option('texto_btn_relatorios_atividade'); ?>" />
					<p style="color: #999999;font-size: 13px;margin: 5px 0 0 5px">Ex: Serviços</p>
				</div>
				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Exibir posts</label>
					<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="post_per_page_relatorios_atividade" type="text" value="<?php echo get_option('post_per_page_relatorios_atividade'); ?>" />
					<p style="color: #999999;font-size: 13px;margin: 5px 0 0 5px">Ex: 10</p>
				</div>
				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Banner</label>
					<?php 
					if(function_exists( 'wp_enqueue_media' )){
						wp_enqueue_media();
					}else{
						wp_enqueue_style('thickbox');
						wp_enqueue_script('media-upload');
						wp_enqueue_script('thickbox');
					} ?>
					<img class="banner_relatorios_atividade" src="<?php echo get_option('banner_relatorios_atividade'); ?>" height="100" width="250" style="margin-bottom: 10px;"/><br>
					<input class="banner_relatorios_atividade_url" type="hidden" name="banner_relatorios_atividade" size="60" value="<?php echo get_option('banner_relatorios_atividade'); ?>">
					<a href="#" class="page-title-action banner_relatorios_atividade_upload">Upload</a>
					<script>
						jQuery(document).ready(function($) {
							$('.banner_relatorios_atividade_upload').click(function(e) {
								e.preventDefault();
								var custom_uploader = wp.media({
									title: 'Custom Image',
									button: {
										text: 'Upload Image'
									},
	                                    multiple: false  // Set this to true to allow multiple files to be selected
	                                })
								.on('select', function() {
									var attachment = custom_uploader.state().get('selection').first().toJSON();
									$('.banner_relatorios_atividade').attr('src', attachment.url);
									$('.banner_relatorios_atividade_url').val(attachment.url);
								})
								.open();
							});
						});
					</script>
				</div>
				<div style="clear:both"></div>
				<p class="submit">
					<input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />
				</p>
			</fieldset>		
		</form>
	</div>
<?php 
}

function wp_conselhopage(){ ?>
	<div class="wrap">
		<div id="icon-tools" class="icon32"></div>
		<form method="post" action="options.php" style="float:left; width:70%">
			<?php settings_fields( 'settings-group-conselho' ); ?>
			<fieldset style=" width:100%; padding:20px; margin:30px;">
				<h2 style="font-weight:bold; font-size:24px;">Inscrever-se</h2>
				<?php if (isset($_GET['settings-updated'])) : ?>
					<div class="notice notice-success is-dismissible"><p>Atualizado com sucesso.</p></div>
				<?php endif; ?>
				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Titulo</label>
					<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="titulo_conselho" type="text" value="<?php echo get_option('titulo_conselho'); ?>" />
					<p style="color: #999999;font-size: 13px;margin: 5px 0 0 5px">Ex: Eternos conselho</p>
				</div>
				
				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Banner</label>
					<?php 
					if(function_exists( 'wp_enqueue_media' )){
						wp_enqueue_media();
					}else{
						wp_enqueue_style('thickbox');
						wp_enqueue_script('media-upload');
						wp_enqueue_script('thickbox');
					} ?>
					<img class="banner_conselho" src="<?php echo get_option('banner_conselho'); ?>" height="100" width="250" style="margin-bottom: 10px;"/><br>
					<input class="banner_conselho_url" type="hidden" name="banner_conselho" size="60" value="<?php echo get_option('banner_conselho'); ?>">
					<a href="#" class="page-title-action banner_conselho_upload">Upload</a>
					<script>
						jQuery(document).ready(function($) {
							$('.banner_conselho_upload').click(function(e) {
								e.preventDefault();
								var custom_uploader = wp.media({
									title: 'Custom Image',
									button: {
										text: 'Upload Image'
									},
	                                    multiple: false  // Set this to true to allow multiple files to be selected
	                                })
								.on('select', function() {
									var attachment = custom_uploader.state().get('selection').first().toJSON();
									$('.banner_conselho').attr('src', attachment.url);
									$('.banner_conselho_url').val(attachment.url);
								})
								.open();
							});
						});
					</script>
				</div>
				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Titulo Fiscal</label>
					<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="titulo_fiscal" type="text" value="<?php echo get_option('titulo_fiscal'); ?>" />
					<p style="color: #999999;font-size: 13px;margin: 5px 0 0 5px">Ex: Eternos expresidentes</p>
				</div>
				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Titulo Executivo</label>
					<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="titulo_executivo" type="text" value="<?php echo get_option('titulo_executivo'); ?>" />
					<p style="color: #999999;font-size: 13px;margin: 5px 0 0 5px">Ex: Eternos expresidentes</p>
				</div>
				<div style="clear:both"></div>
				<p class="submit">
					<input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />
				</p>
			</fieldset>		
		</form>
	</div>
<?php 
}

function wp_noticiaspage(){ ?>
	<div class="wrap">
		<div id="icon-tools" class="icon32"></div>
		<form method="post" action="options.php" style="float:left; width:70%">
			<?php settings_fields( 'settings-group-noticias' ); ?>
			<fieldset style=" width:100%; padding:20px; margin:30px;">
				<h2 style="font-weight:bold; font-size:24px;">Dados notícias</h2>
				<?php if (isset($_GET['settings-updated'])) : ?>
					<div class="notice notice-success is-dismissible"><p>Atualizado com sucesso.</p></div>
				<?php endif; ?>

				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Titulo - Últimas Notícias</label>
					<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="titulo_primeiro_noticias" type="text" value="<?php echo get_option('titulo_primeiro_noticias'); ?>" />
					
				</div>			
				
				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Titulo - Notícias</label>
					<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="titulo_segundo_noticias" type="text" value="<?php echo get_option('titulo_segundo_noticias'); ?>" />
					
				</div>

				<div style="clear:both"></div>
				<p class="submit">
					<input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />
				</p>
			</fieldset>		
		</form>
	</div>
<?php 
}

function wp_eventospage(){ ?>
	<div class="wrap">
		<div id="icon-tools" class="icon32"></div>
		<form method="post" action="options.php" style="float:left; width:70%">
			<?php settings_fields( 'settings-group-eventos' ); ?>
			<fieldset style=" width:100%; padding:20px; margin:30px;">
				<h2 style="font-weight:bold; font-size:24px;">Dados Eventos</h2>
				<?php if (isset($_GET['settings-updated'])) : ?>
					<div class="notice notice-success is-dismissible"><p>Atualizado com sucesso.</p></div>
				<?php endif; ?>
				
				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Exibir posts</label>
					<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="post_per_page_eventos" type="text" value="<?php echo get_option('post_per_page_eventos'); ?>" />
					<p style="color: #999999;font-size: 13px;margin: 5px 0 0 5px">Ex: 10</p>
				</div>
				
				<div style="clear:both"></div>
				<p class="submit">
					<input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />
				</p>
			</fieldset>		
		</form>
	</div>
<?php 
}
function wp_expresidentespage(){ ?>
	<div class="wrap">
		<div id="icon-tools" class="icon32"></div>
		<form method="post" action="options.php" style="float:left; width:70%">
			<?php settings_fields( 'settings-group-expresidentes' ); ?>
			<fieldset style=" width:100%; padding:20px; margin:30px;">
				<h2 style="font-weight:bold; font-size:24px;">Dados Ex Presidentes</h2>
				<?php if (isset($_GET['settings-updated'])) : ?>
					<div class="notice notice-success is-dismissible"><p>Atualizado com sucesso.</p></div>
				<?php endif; ?>
				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Titulo</label>
					<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="titulo_expresidentes" type="text" value="<?php echo get_option('titulo_expresidentes'); ?>" />
					<p style="color: #999999;font-size: 13px;margin: 5px 0 0 5px">Ex: Eternos expresidentes</p>
				</div>
				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Exibir posts</label>
					<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="post_per_page_expresidentes" type="text" value="<?php echo get_option('post_per_page_expresidentes'); ?>" />
					<p style="color: #999999;font-size: 13px;margin: 5px 0 0 5px">Ex: 10</p>
				</div>
				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Banner</label>
					<?php 
					if(function_exists( 'wp_enqueue_media' )){
						wp_enqueue_media();
					}else{
						wp_enqueue_style('thickbox');
						wp_enqueue_script('media-upload');
						wp_enqueue_script('thickbox');
					} ?>
					<img class="banner_expresidentes" src="<?php echo get_option('banner_expresidentes'); ?>" height="100" width="250" style="margin-bottom: 10px;"/><br>
					<input class="banner_expresidentes_url" type="hidden" name="banner_expresidentes" size="60" value="<?php echo get_option('banner_expresidentes'); ?>">
					<a href="#" class="page-title-action banner_expresidentes_upload">Upload</a>
					<script>
						jQuery(document).ready(function($) {
							$('.banner_expresidentes_upload').click(function(e) {
								e.preventDefault();
								var custom_uploader = wp.media({
									title: 'Custom Image',
									button: {
										text: 'Upload Image'
									},
	                                    multiple: false  // Set this to true to allow multiple files to be selected
	                                })
								.on('select', function() {
									var attachment = custom_uploader.state().get('selection').first().toJSON();
									$('.banner_expresidentes').attr('src', attachment.url);
									$('.banner_expresidentes_url').val(attachment.url);
								})
								.open();
							});
						});
					</script>
				</div>
				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Titulo coluna Ex presidentes</label>
					<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="titulo_nomes" type="text" value="<?php echo get_option('titulo_nomes'); ?>" />
					<p style="color: #999999;font-size: 13px;margin: 5px 0 0 5px">Ex: Eternos expresidentes</p>
				</div>
				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Titulo coluna Mandatos</label>
					<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="titulo_mandatos" type="text" value="<?php echo get_option('titulo_mandatos'); ?>" />
					<p style="color: #999999;font-size: 13px;margin: 5px 0 0 5px">Ex: Eternos expresidentes</p>
				</div>
				<div style="clear:both"></div>
				<p class="submit">
					<input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />
				</p>
			</fieldset>		
		</form>
	</div>
<?php 
}
function wp_colegaspage(){ ?>
	<div class="wrap">
		<div id="icon-tools" class="icon32"></div>
		<form method="post" action="options.php" style="float:left; width:70%">
			<?php settings_fields( 'settings-group-colegas' ); ?>
			<fieldset style=" width:100%; padding:20px; margin:30px;">
				<h2 style="font-weight:bold; font-size:24px;">Dados Eternos Colegas</h2>
				<?php if (isset($_GET['settings-updated'])) : ?>
					<div class="notice notice-success is-dismissible"><p>Atualizado com sucesso.</p></div>
				<?php endif; ?>
				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Titulo</label>
					<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="titulo_colegas" type="text" value="<?php echo get_option('titulo_colegas'); ?>" />
					<p style="color: #999999;font-size: 13px;margin: 5px 0 0 5px">Ex: Eternos Colegas</p>
				</div>
				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Exibir posts</label>
					<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="post_per_page_colegas" type="text" value="<?php echo get_option('post_per_page_colegas'); ?>" />
					<p style="color: #999999;font-size: 13px;margin: 5px 0 0 5px">Ex: 10</p>
				</div>

				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Banner</label>
					<?php 
					if(function_exists( 'wp_enqueue_media' )){
						wp_enqueue_media();
					}else{
						wp_enqueue_style('thickbox');
						wp_enqueue_script('media-upload');
						wp_enqueue_script('thickbox');
					} ?>
					<img class="banner_colegas" src="<?php echo get_option('banner_colegas'); ?>" height="100" width="250" style="margin-bottom: 10px;"/><br>
					<input class="banner_colegas_url" type="hidden" name="banner_colegas" size="60" value="<?php echo get_option('banner_colegas'); ?>">
					<a href="#" class="page-title-action banner_colegas_upload">Upload</a>
					<script>
						jQuery(document).ready(function($) {
							$('.banner_colegas_upload').click(function(e) {
								e.preventDefault();
								var custom_uploader = wp.media({
									title: 'Custom Image',
									button: {
										text: 'Upload Image'
									},
	                                    multiple: false  // Set this to true to allow multiple files to be selected
	                                })
								.on('select', function() {
									var attachment = custom_uploader.state().get('selection').first().toJSON();
									$('.banner_colegas').attr('src', attachment.url);
									$('.banner_colegas_url').val(attachment.url);
								})
								.open();
							});
						});
					</script>
				</div>
				<div style="clear:both"></div>
				<p class="submit">
					<input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />
				</p>
			</fieldset>		
		</form>
	</div>
<?php 
}

function wp_patrimoniaispage(){ ?>
	<div class="wrap">
		<div id="icon-tools" class="icon32"></div>
		<form method="post" action="options.php" style="float:left; width:70%">
			<?php settings_fields( 'settings-group-patrimoniais' ); ?>
			<fieldset style=" width:100%; padding:20px; margin:30px;">
				<h2 style="font-weight:bold; font-size:24px;">Dados Balanço Patrimonial</h2>
				<?php if (isset($_GET['settings-updated'])) : ?>
					<div class="notice notice-success is-dismissible"><p>Atualizado com sucesso.</p></div>
				<?php endif; ?>
				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Título Banner</label>
					<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="titulo_patrimoniais" type="text" value="<?php echo get_option('titulo_patrimoniais'); ?>" />
					<p style="color: #999999;font-size: 13px;margin: 5px 0 0 5px">Ex: Serviços</p>
				</div>
				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Texto Botão</label>
					<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="texto_btn_patrimoniais" type="text" value="<?php echo get_option('texto_btn_patrimoniais'); ?>" />
					<p style="color: #999999;font-size: 13px;margin: 5px 0 0 5px">Ex: Serviços</p>
				</div>
				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Exibir posts</label>
					<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="post_per_page_patrimoniais" type="text" value="<?php echo get_option('post_per_page_patrimoniais'); ?>" />
					<p style="color: #999999;font-size: 13px;margin: 5px 0 0 5px">Ex: 10</p>
				</div>
				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Banner</label>
					<?php 
					if(function_exists( 'wp_enqueue_media' )){
						wp_enqueue_media();
					}else{
						wp_enqueue_style('thickbox');
						wp_enqueue_script('media-upload');
						wp_enqueue_script('thickbox');
					} ?>
					<img class="banner_patrimoniais" src="<?php echo get_option('banner_patrimoniais'); ?>" height="100" width="250" style="margin-bottom: 10px;"/><br>
					<input class="banner_patrimoniais_url" type="hidden" name="banner_patrimoniais" size="60" value="<?php echo get_option('banner_patrimoniais'); ?>">
					<a href="#" class="page-title-action banner_patrimoniais_upload">Upload</a>
					<script>
						jQuery(document).ready(function($) {
							$('.banner_patrimoniais_upload').click(function(e) {
								e.preventDefault();
								var custom_uploader = wp.media({
									title: 'Custom Image',
									button: {
										text: 'Upload Image'
									},
	                                    multiple: false  // Set this to true to allow multiple files to be selected
	                                })
								.on('select', function() {
									var attachment = custom_uploader.state().get('selection').first().toJSON();
									$('.banner_patrimoniais').attr('src', attachment.url);
									$('.banner_patrimoniais_url').val(attachment.url);
								})
								.open();
							});
						});
					</script>
				</div>
				<div style="clear:both"></div>
				<p class="submit">
					<input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />
				</p>
			</fieldset>		
		</form>
	</div>
<?php 
}
function wp_literariospage(){ ?>
	<div class="wrap">
		<div id="icon-tools" class="icon32"></div>
		<form method="post" action="options.php" style="float:left; width:70%">
			<?php settings_fields( 'settings-group-literarios' ); ?>
			<fieldset style=" width:100%; padding:20px; margin:30px;">
				<h2 style="font-weight:bold; font-size:24px;">Dados Espaço Literário</h2>
				<?php if (isset($_GET['settings-updated'])) : ?>
					<div class="notice notice-success is-dismissible"><p>Atualizado com sucesso.</p></div>
				<?php endif; ?>
				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Título Banner</label>
					<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="titulo_literarios" type="text" value="<?php echo get_option('titulo_literarios'); ?>" />
					<p style="color: #999999;font-size: 13px;margin: 5px 0 0 5px">Ex: Serviços</p>
				</div>
				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Texto Botão</label>
					<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="texto_btn_literarios" type="text" value="<?php echo get_option('texto_btn_literarios'); ?>" />
					<p style="color: #999999;font-size: 13px;margin: 5px 0 0 5px">Ex: Serviços</p>
				</div>
				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Exibir posts</label>
					<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="post_per_page_literarios" type="text" value="<?php echo get_option('post_per_page_literarios'); ?>" />
					<p style="color: #999999;font-size: 13px;margin: 5px 0 0 5px">Ex: 10</p>
				</div>
				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Banner</label>
					<?php 
					if(function_exists( 'wp_enqueue_media' )){
						wp_enqueue_media();
					}else{
						wp_enqueue_style('thickbox');
						wp_enqueue_script('media-upload');
						wp_enqueue_script('thickbox');
					} ?>
					<img class="banner_literarios" src="<?php echo get_option('banner_literarios'); ?>" height="100" width="250" style="margin-bottom: 10px;"/><br>
					<input class="banner_literarios_url" type="hidden" name="banner_literarios" size="60" value="<?php echo get_option('banner_literarios'); ?>">
					<a href="#" class="page-title-action banner_literarios_upload">Upload</a>
					<script>
						jQuery(document).ready(function($) {
							$('.banner_literarios_upload').click(function(e) {
								e.preventDefault();
								var custom_uploader = wp.media({
									title: 'Custom Image',
									button: {
										text: 'Upload Image'
									},
	                                    multiple: false  // Set this to true to allow multiple files to be selected
	                                })
								.on('select', function() {
									var attachment = custom_uploader.state().get('selection').first().toJSON();
									$('.banner_literarios').attr('src', attachment.url);
									$('.banner_literarios_url').val(attachment.url);
								})
								.open();
							});
						});
					</script>
				</div>
				<div style="clear:both"></div>
				<p class="submit">
					<input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />
				</p>
			</fieldset>		
		</form>
	</div>
<?php 
}

function settingsNewsletter(){ ?>
	<div class="wrap">
		<div id="icon-tools" class="icon32"></div>
		<form method="post" action="options.php" style="float:left; width:70%">
			<?php settings_fields( 'settings-group-newsletter' ); ?>
			<fieldset style=" width:100%; padding:20px; margin:30px;">
				<h2 style="font-weight:bold; font-size:24px;">Dados Newsletter</h2>
				<?php if (isset($_GET['settings-updated'])) : ?>
					<div class="notice notice-success is-dismissible"><p>Atualizado com sucesso.</p></div>
				<?php endif; ?>		

					<div class="clear" style="margin-bottom:20px">
						<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Texto</label>
						<textarea style="width:100%; background:#fff; border-radius:5px; height:100px" name="texto_form_inscrever" type="text"><?php echo get_option('texto_form_inscrever'); ?></textarea>						
					</div>
					
					<div class="clear" style="margin-bottom:20px">
						<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Imagem de fundo</label>
						<?php 
						if(function_exists( 'wp_enqueue_media' )){
							wp_enqueue_media();
						}else{
							wp_enqueue_style('thickbox');
							wp_enqueue_script('media-upload');
							wp_enqueue_script('thickbox');
						} ?>
						<img class="imagem_form_inscrever" src="<?php echo get_option('imagem_form_inscrever'); ?>" height="100" width="250" style="margin-bottom: 10px;"/><br>
						<input class="imagem_form_inscrever_url" type="hidden" name="imagem_form_inscrever" size="60" value="<?php echo get_option('imagem_form_inscrever'); ?>">
						<a href="#" class="page-title-action imagem_form_inscrever_upload">Upload</a>
						<script>
							jQuery(document).ready(function($) {
								$('.imagem_form_inscrever_upload').click(function(e) {
									e.preventDefault();
									var custom_uploader = wp.media({
										title: 'Custom Image',
										button: {
											text: 'Upload Image'
										},
                                    multiple: false  // Set this to true to allow multiple files to be selected
                                })
									.on('select', function() {
										var attachment = custom_uploader.state().get('selection').first().toJSON();
										$('.imagem_form_inscrever').attr('src', attachment.url);
										$('.imagem_form_inscrever_url').val(attachment.url);
									})
									.open();
								});
							});
						</script>

					</div>

				

				

				
				
				<div style="clear:both"></div>
				<p class="submit">
					<input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />
				</p>
			</fieldset>		
		</form>
	</div>
<?php 
}

function wp_videospage(){ ?>
	<div class="wrap">
		<div id="icon-tools" class="icon32"></div>
		<form method="post" action="options.php" style="float:left; width:70%">
			<?php settings_fields( 'settings-group-videos' ); ?>
			<fieldset style=" width:100%; padding:20px; margin:30px;">
				<h2 style="font-weight:bold; font-size:24px;">Dados Vídeos</h2>
				<?php if (isset($_GET['settings-updated'])) : ?>
					<div class="notice notice-success is-dismissible"><p>Atualizado com sucesso.</p></div>
				<?php endif; ?>
				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Título Banner</label>
					<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="titulo_videos" type="text" value="<?php echo get_option('titulo_videos'); ?>" />
					<p style="color: #999999;font-size: 13px;margin: 5px 0 0 5px">Ex: Serviços</p>
				</div>
				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Texto Botão</label>
					<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="texto_btn_videos" type="text" value="<?php echo get_option('texto_btn_videos'); ?>" />
					<p style="color: #999999;font-size: 13px;margin: 5px 0 0 5px">Ex: Serviços</p>
				</div>
				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Exibir posts</label>
					<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="post_per_page_videos" type="text" value="<?php echo get_option('post_per_page_videos'); ?>" />
					<p style="color: #999999;font-size: 13px;margin: 5px 0 0 5px">Ex: 10</p>
				</div>
				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Banner</label>
					<?php 
					if(function_exists( 'wp_enqueue_media' )){
						wp_enqueue_media();
					}else{
						wp_enqueue_style('thickbox');
						wp_enqueue_script('media-upload');
						wp_enqueue_script('thickbox');
					} ?>
					<img class="banner_videos" src="<?php echo get_option('banner_videos'); ?>" height="100" width="250" style="margin-bottom: 10px;"/><br>
					<input class="banner_videos_url" type="hidden" name="banner_videos" size="60" value="<?php echo get_option('banner_videos'); ?>">
					<a href="#" class="page-title-action banner_videos_upload">Upload</a>
					<script>
						jQuery(document).ready(function($) {
							$('.banner_videos_upload').click(function(e) {
								e.preventDefault();
								var custom_uploader = wp.media({
									title: 'Custom Image',
									button: {
										text: 'Upload Image'
									},
	                                    multiple: false  // Set this to true to allow multiple files to be selected
	                                })
								.on('select', function() {
									var attachment = custom_uploader.state().get('selection').first().toJSON();
									$('.banner_videos').attr('src', attachment.url);
									$('.banner_videos_url').val(attachment.url);
								})
								.open();
							});
						});
					</script>
				</div>
				<div style="clear:both"></div>
				<p class="submit">
					<input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />
				</p>
			</fieldset>		
		</form>
	</div>
<?php 
}
function wp_publicacoespage(){ ?>
	<div class="wrap">
		<div id="icon-tools" class="icon32"></div>
		<form method="post" action="options.php" style="float:left; width:70%">
			<?php settings_fields( 'settings-group-publicacoes' ); ?>
			<fieldset style=" width:100%; padding:20px; margin:30px;">
				<h2 style="font-weight:bold; font-size:24px;">Dados publicações</h2>
				<?php if (isset($_GET['settings-updated'])) : ?>
					<div class="notice notice-success is-dismissible"><p>Atualizado com sucesso.</p></div>
				<?php endif; ?>
				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Título Banner</label>
					<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="titulo_publicacoes" type="text" value="<?php echo get_option('titulo_publicacoes'); ?>" />
					<p style="color: #999999;font-size: 13px;margin: 5px 0 0 5px">Ex: Serviços</p>
				</div>
				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Texto Botão Ver mais</label>
					<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="texto_btn_publicacao" type="text" value="<?php echo get_option('texto_btn_publicacao'); ?>" />
					<p style="color: #999999;font-size: 13px;margin: 5px 0 0 5px">Ex: Serviços</p>
				</div>
				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Texto Botão Download</label>
					<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="txt_btn_pub_download" type="text" value="<?php echo get_option('txt_btn_pub_download'); ?>" />
					<p style="color: #999999;font-size: 13px;margin: 5px 0 0 5px">Ex: Serviços</p>
				</div>
				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Exibir posts</label>
					<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="post_per_page_publicacoes" type="text" value="<?php echo get_option('post_per_page_publicacoes'); ?>" />
					<p style="color: #999999;font-size: 13px;margin: 5px 0 0 5px">Ex: 10</p>
				</div>
				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Banner</label>
					<?php 
					if(function_exists( 'wp_enqueue_media' )){
						wp_enqueue_media();
					}else{
						wp_enqueue_style('thickbox');
						wp_enqueue_script('media-upload');
						wp_enqueue_script('thickbox');
					} ?>
					<img class="banner_publicacoes" src="<?php echo get_option('banner_publicacoes'); ?>" height="100" width="250" style="margin-bottom: 10px;"/><br>
					<input class="banner_publicacoes_url" type="hidden" name="banner_publicacoes" size="60" value="<?php echo get_option('banner_publicacoes'); ?>">
					<a href="#" class="page-title-action banner_publicacoes_upload">Upload</a>
					<script>
						jQuery(document).ready(function($) {
							$('.banner_publicacoes_upload').click(function(e) {
								e.preventDefault();
								var custom_uploader = wp.media({
									title: 'Custom Image',
									button: {
										text: 'Upload Image'
									},
	                                    multiple: false  // Set this to true to allow multiple files to be selected
	                                })
								.on('select', function() {
									var attachment = custom_uploader.state().get('selection').first().toJSON();
									$('.banner_publicacoes').attr('src', attachment.url);
									$('.banner_publicacoes_url').val(attachment.url);
								})
								.open();
							});
						});
					</script>
				</div>
				<h2 style="font-weight:bold; font-size:24px;">Publicações Home</h2>
				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Banner Home</label>
					<?php 
					if(function_exists( 'wp_enqueue_media' )){
						wp_enqueue_media();
					}else{
						wp_enqueue_style('thickbox');
						wp_enqueue_script('media-upload');
						wp_enqueue_script('thickbox');
					} ?>
					<img class="banner_home" src="<?php echo get_option('banner_home'); ?>" height="100" width="250" style="margin-bottom: 10px;"/><br>
					<input class="banner_home_url" type="hidden" name="banner_home" size="60" value="<?php echo get_option('banner_home'); ?>">
					<a href="#" class="page-title-action banner_home_upload">Upload</a>
					<script>
						jQuery(document).ready(function($) {
							$('.banner_home_upload').click(function(e) {
								e.preventDefault();
								var custom_uploader = wp.media({
									title: 'Custom Image',
									button: {
										text: 'Upload Image'
									},
	                                    multiple: false  // Set this to true to allow multiple files to be selected
	                                })
								.on('select', function() {
									var attachment = custom_uploader.state().get('selection').first().toJSON();
									$('.banner_home').attr('src', attachment.url);
									$('.banner_home_url').val(attachment.url);
								})
								.open();
							});
						});
					</script>
				</div>
				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Icone Banner Home</label>
					<?php 
					if(function_exists( 'wp_enqueue_media' )){
						wp_enqueue_media();
					}else{
						wp_enqueue_style('thickbox');
						wp_enqueue_script('media-upload');
						wp_enqueue_script('thickbox');
					} ?>
					<img class="icone_banner_home" src="<?php echo get_option('icone_banner_home'); ?>" height="100" width="250" style="margin-bottom: 10px;"/><br>
					<input class="icone_banner_home_url" type="hidden" name="icone_banner_home" size="60" value="<?php echo get_option('icone_banner_home'); ?>">
					<a href="#" class="page-title-action icone_banner_home_upload">Upload</a>
					<script>
						jQuery(document).ready(function($) {
							$('.icone_banner_home_upload').click(function(e) {
								e.preventDefault();
								var custom_uploader = wp.media({
									title: 'Custom Image',
									button: {
										text: 'Upload Image'
									},
	                                    multiple: false  // Set this to true to allow multiple files to be selected
	                                })
								.on('select', function() {
									var attachment = custom_uploader.state().get('selection').first().toJSON();
									$('.icone_banner_home').attr('src', attachment.url);
									$('.icone_banner_home_url').val(attachment.url);
								})
								.open();
							});
						});
					</script>
				</div>
				<div style="clear:both"></div>
				<p class="submit">
					<input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />
				</p>
			</fieldset>		
		</form>
	</div>
<?php 
}

function wp_artigospage(){ ?>
	<div class="wrap">
		<div id="icon-tools" class="icon32"></div>
		<form method="post" action="options.php" style="float:left; width:70%">
			<?php settings_fields( 'settings-group-artigos' ); ?>
			<fieldset style=" width:100%; padding:20px; margin:30px;">
				<h2 style="font-weight:bold; font-size:24px;">Dados artigos</h2>
				<?php if (isset($_GET['settings-updated'])) : ?>
					<div class="notice notice-success is-dismissible"><p>Atualizado com sucesso.</p></div>
				<?php endif; ?>
				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Título Banner</label>
					<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="titulo_artigos" type="text" value="<?php echo get_option('titulo_artigos'); ?>" />
					<p style="color: #999999;font-size: 13px;margin: 5px 0 0 5px">Ex: Serviços</p>
				</div>
				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Texto Botão</label>
					<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="texto_btn_artigo" type="text" value="<?php echo get_option('texto_btn_artigo'); ?>" />
					<p style="color: #999999;font-size: 13px;margin: 5px 0 0 5px">Ex: Serviços</p>
				</div>
				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Exibir posts</label>
					<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="post_per_page_artigos" type="text" value="<?php echo get_option('post_per_page_artigos'); ?>" />
					<p style="color: #999999;font-size: 13px;margin: 5px 0 0 5px">Ex: 10</p>
				</div>
				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Banner</label>
					<?php 
					if(function_exists( 'wp_enqueue_media' )){
						wp_enqueue_media();
					}else{
						wp_enqueue_style('thickbox');
						wp_enqueue_script('media-upload');
						wp_enqueue_script('thickbox');
					} ?>
					<img class="banner_artigos" src="<?php echo get_option('banner_artigos'); ?>" height="100" width="250" style="margin-bottom: 10px;"/><br>
					<input class="banner_artigos_url" type="hidden" name="banner_artigos" size="60" value="<?php echo get_option('banner_artigos'); ?>">
					<a href="#" class="page-title-action banner_artigos_upload">Upload</a>
					<script>
						jQuery(document).ready(function($) {
							$('.banner_artigos_upload').click(function(e) {
								e.preventDefault();
								var custom_uploader = wp.media({
									title: 'Custom Image',
									button: {
										text: 'Upload Image'
									},
	                                    multiple: false  // Set this to true to allow multiple files to be selected
	                                })
								.on('select', function() {
									var attachment = custom_uploader.state().get('selection').first().toJSON();
									$('.banner_artigos').attr('src', attachment.url);
									$('.banner_artigos_url').val(attachment.url);
								})
								.open();
							});
						});
					</script>
				</div>
				<div style="clear:both"></div>
				<p class="submit">
					<input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />
				</p>
			</fieldset>		
		</form>
	</div>
<?php 
}


function wp_fotospage(){ ?>
	<div class="wrap">
		<div id="icon-tools" class="icon32"></div>
		<form method="post" action="options.php" style="float:left; width:70%">
			<?php settings_fields( 'settings-group-fotos' ); ?>
			<fieldset style=" width:100%; padding:20px; margin:30px;">
				<h2 style="font-weight:bold; font-size:24px;">Dados fotos</h2>
				<?php if (isset($_GET['settings-updated'])) : ?>
					<div class="notice notice-success is-dismissible"><p>Atualizado com sucesso.</p></div>
				<?php endif; ?>
				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Título Banner</label>
					<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="titulo_fotos" type="text" value="<?php echo get_option('titulo_fotos'); ?>" />
					<p style="color: #999999;font-size: 13px;margin: 5px 0 0 5px">Ex: Serviços</p>
				</div>
				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Exibir posts</label>
					<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="post_per_page_fotos" type="text" value="<?php echo get_option('post_per_page_fotos'); ?>" />
					<p style="color: #999999;font-size: 13px;margin: 5px 0 0 5px">Ex: 10</p>
				</div>
				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Banner</label>
					<?php 
					if(function_exists( 'wp_enqueue_media' )){
						wp_enqueue_media();
					}else{
						wp_enqueue_style('thickbox');
						wp_enqueue_script('media-upload');
						wp_enqueue_script('thickbox');
					} ?>
					<img class="banner_fotos" src="<?php echo get_option('banner_fotos'); ?>" height="100" width="250" style="margin-bottom: 10px;"/><br>
					<input class="banner_fotos_url" type="hidden" name="banner_fotos" size="60" value="<?php echo get_option('banner_fotos'); ?>">
					<a href="#" class="page-title-action banner_fotos_upload">Upload</a>
					<script>
						jQuery(document).ready(function($) {
							$('.banner_fotos_upload').click(function(e) {
								e.preventDefault();
								var custom_uploader = wp.media({
									title: 'Custom Image',
									button: {
										text: 'Upload Image'
									},
	                                    multiple: false  // Set this to true to allow multiple files to be selected
	                                })
								.on('select', function() {
									var attachment = custom_uploader.state().get('selection').first().toJSON();
									$('.banner_fotos').attr('src', attachment.url);
									$('.banner_fotos_url').val(attachment.url);
								})
								.open();
							});
						});
					</script>
				</div>
				<div style="clear:both"></div>
				<p class="submit">
					<input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />
				</p>
			</fieldset>		
		</form>
	</div>
<?php 
}


function settingsSobre_page() {
	?>
	<div class="wrap">
		<!--area exibida no painel no wordpress-->
		<form method="post" action="options.php" style="float:left; width:70%">
			<?php settings_fields( 'settings-group-sobre' ); ?>
			<fieldset style=" width:100%; padding:20px; margin:30px;">
				<h2 style="font-weight:bold; font-size:24px;">Contato</h2>

				<?php if (isset($_GET['settings-updated'])) : ?>
					<div class="notice notice-success is-dismissible"><p>Atualizado com sucesso.</p></div>
				<?php endif; ?>

				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Banner</label>
					<?php 
					if(function_exists( 'wp_enqueue_media' )){
						wp_enqueue_media();
					}else{
						wp_enqueue_style('thickbox');
						wp_enqueue_script('media-upload');
						wp_enqueue_script('thickbox');
					} ?>
					<img class="banner_contato" src="<?php echo get_option('banner_contato'); ?>" height="100" width="250" style="margin-bottom: 10px;"/><br>
					<input class="banner_contato_url" type="hidden" name="banner_contato" size="60" value="<?php echo get_option('banner_contato'); ?>">
					<a href="#" class="page-title-action banner_contato_upload">Upload</a>
					<script>
						jQuery(document).ready(function($) {
							$('.banner_contato_upload').click(function(e) {
								e.preventDefault();
								var custom_uploader = wp.media({
									title: 'Custom Image',
									button: {
										text: 'Upload Image'
									},
	                                    multiple: false  // Set this to true to allow multiple files to be selected
	                                })
								.on('select', function() {
									var attachment = custom_uploader.state().get('selection').first().toJSON();
									$('.banner_contato').attr('src', attachment.url);
									$('.banner_contato_url').val(attachment.url);
								})
								.open();
							});
						});
					</script>
				</div>
			
				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Título</label>
					<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="titulo_contato" type="text" value="<?php echo get_option('titulo_contato'); ?>" />
					<p style="color: #999999;font-size: 11px;margin: 5px 0 0 5px">Fale Conosco</p>
				</div>

				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Título</label>
					<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="sub_titulo_contato" type="text" value="<?php echo get_option('sub_titulo_contato'); ?>" />
					<p style="color: #999999;font-size: 11px;margin: 5px 0 0 5px">Fale Conosco</p>
				</div>

				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">E-Mail</label>
					<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="email" type="text" value="<?php echo get_option('email'); ?>" />
					<p style="color: #999999;font-size: 11px;margin: 5px 0 0 5px">Ex:contato@maiscode.com.br</p>
				</div>

				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Telefone</label>
					<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="telefone" type="text" value="<?php echo get_option('telefone'); ?>" />
					<p style="color: #999999;font-size: 11px;margin: 5px 0 0 5px">Ex: (99) 9 9999-9999</p>
				</div>

				<h2 style="font-weight:bold; font-size:24px;">Endereço</h2>

				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Endereço</label>
					<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="endereco" type="text" value="<?php echo get_option('endereco'); ?>" />
					<p style="color: #999999;font-size: 11px;margin: 5px 0 0 5px">Ex: Endereço</p>
				</div>

				<h2 style="font-weight:bold; font-size:24px;">Cabeçalho</h2>
					<div class="clear" style="margin-bottom:20px">
						<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Logo</label>
						<?php 
						if(function_exists( 'wp_enqueue_media' )){
							wp_enqueue_media();
						}else{
							wp_enqueue_style('thickbox');
							wp_enqueue_script('media-upload');
							wp_enqueue_script('thickbox');
						} ?>
						<img class="header_logo" src="<?php echo get_option('header_logo'); ?>" height="100" width="250" style="margin-bottom: 10px;"/><br>
						<input class="header_logo_url" type="hidden" name="header_logo" size="60" value="<?php echo get_option('header_logo'); ?>">
						<a href="#" class="page-title-action header_logo_upload">Upload</a>
						<script>
							jQuery(document).ready(function($) {
								$('.header_logo_upload').click(function(e) {
									e.preventDefault();
									var custom_uploader = wp.media({
										title: 'Custom Image',
										button: {
											text: 'Upload Image'
										},
                                    multiple: false  // Set this to true to allow multiple files to be selected
                                })
									.on('select', function() {
										var attachment = custom_uploader.state().get('selection').first().toJSON();
										$('.header_logo').attr('src', attachment.url);
										$('.header_logo_url').val(attachment.url);
									})
									.open();
								});
							});
						</script>

					</div>

					<div class="clear" style="margin-bottom:20px">
						<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Título Recuperar Senha</label>
						<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="titulo_form_recupera_senha" type="text" value="<?php echo get_option('titulo_form_recupera_senha'); ?>" />
						<p style="color: #999999;font-size: 11px;margin: 5px 0 0 5px">Sobre a ANFIP-MG</p>
					</div>
					<div class="clear" style="margin-bottom:20px">
						<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Texto Recuperar Senha</label>
						<textarea style="width:100%; background:#fff; border-radius:5px; height:100px" name="texto_form_recupera_senha" type="text"><?php echo get_option('texto_form_recupera_senha'); ?></textarea>
						<p style="color: #999999;font-size: 11px;margin: 5px 0 0 5px">Sobre a ANFIP-MG</p>
					</div>
					<div class="clear" style="margin-bottom:20px">
						<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Título Cadastro</label>
						<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="titulo_form_cadastro" type="text" value="<?php echo get_option('titulo_form_cadastro'); ?>" />
						<p style="color: #999999;font-size: 11px;margin: 5px 0 0 5px">Sobre a ANFIP-MG</p>
					</div>
					<div class="clear" style="margin-bottom:20px">
						<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Texto Cadastro</label>
						<textarea style="width:100%; background:#fff; border-radius:5px; height:100px" name="texto_form_cadastro" type="text"><?php echo get_option('texto_form_cadastro'); ?></textarea>
						<p style="color: #999999;font-size: 11px;margin: 5px 0 0 5px">Sobre a ANFIP-MG</p>
					</div>					
					
					<h2 style="font-weight:bold; font-size:24px;">Footer</h2>

					<div class="clear" style="margin-bottom:20px">
						<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Título ANFIP footer</label>
						<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="titulo_anfip_footer" type="text" value="<?php echo get_option('titulo_anfip_footer'); ?>" />
						<p style="color: #999999;font-size: 11px;margin: 5px 0 0 5px">Sobre a ANFIP-MG</p>
					</div>
					<div class="clear" style="margin-bottom:20px">
						<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Texto ANFIP footer</label>
						<textarea style="width:100%; background:#fff; border-radius:5px; height:100px" name="texto_anfip_footer" type="text"><?php echo get_option('texto_anfip_footer'); ?></textarea>
						<p style="color: #999999;font-size: 11px;margin: 5px 0 0 5px">Sobre a ANFIP-MG</p>
					</div>
					<div class="clear" style="margin-bottom:20px">
						<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Texto Rodape</label>
						<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="texto_rodape" type="text" value="<?php echo get_option('texto_rodape'); ?>" />
						<p style="color: #999999;font-size: 11px;margin: 5px 0 0 5px">Sobre a ANFIP-MG</p>
					</div>


				<div style="clear:both"></div>
				<p class="submit">
					<input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />
				</p>

			</fieldset>		

		</form>
	</div>

	<?php };




	function settingsSocial_page() {
		?>
		<div class="wrap">
			<!--area exibida no painel no wordpress-->
			<form method="post" action="options.php" style="float:left; width:70%">
				<?php settings_fields( 'settings-group' ); ?>
				<fieldset style=" width:100%; padding:20px; margin:30px;">

					<h2 style="font-weight:bold; font-size:24px;">Configurações de Redes Sociais</h2>

					<?php if (isset($_GET['settings-updated'])) : ?>
						<div class="notice notice-success is-dismissible"><p>Atualizado com sucesso.</p></div>
					<?php endif; ?>

				
					<div class="clear" style="margin-bottom:20px">
						<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Twitter</label>
						<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="twitter" type="text" value="<?php echo get_option('twitter'); ?>" />
						<p style="color: #999999;font-size: 11px;margin: 5px 0 0 5px">Ex: http://www.twitter.com/</p>
					</div>
					<div class="clear" style="margin-bottom:20px">
						<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Facebook</label>
						<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="facebook" type="text" value="<?php echo get_option('facebook'); ?>" />
						<p style="color: #999999;font-size: 11px;margin: 5px 0 0 5px">Ex: http://www.facebook.com/</p>
					</div>

					<div class="clear" style="margin-bottom:20px">
						<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Youtube</label>
						<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="youtube" type="text" value="<?php echo get_option('youtube'); ?>" />
						<p style="color: #999999;font-size: 11px;margin: 5px 0 0 5px">Ex: http://www.youtube.com/</p>
					</div>
					<div class="clear" style="margin-bottom:20px">
						<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Instagram</label>
						<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="instagram" type="text" value="<?php echo get_option('instagram'); ?>" />
						<p style="color: #999999;font-size: 11px;margin: 5px 0 0 5px">Ex: http://www.instagram.com/</p>
					</div>
					<div class="clear" style="margin-bottom:20px">
						<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">LinkedIn</label>
						<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="linkedin" type="text" value="<?php echo get_option('linkedin'); ?>" />
						<p style="color: #999999;font-size: 11px;margin: 5px 0 0 5px">Ex: http://www.linkedin.com/</p>
					</div>

					<div class="clear" style="margin-bottom:20px">
						<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Texto redes sociais</label>
						<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="texto_redes_sociais" type="text" value="<?php echo get_option('texto_redes_sociais'); ?>" />
						<p style="color: #999999;font-size: 11px;margin: 5px 0 0 5px">Ex: Encontre-me também nas redes sociais e fique por dentro de todas as novidades e conteúdos exclusivos de cada plataforma.</p>
					</div>



					<div style="clear:both"></div>
					<p class="submit">
						<input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />
					</p>
				</fieldset>		

			</form>
		</div>
		<?php };

	// Getter
		function redes_sociais(){
			$facebook = get_option('facebook'); 
			$twitter = get_option('twitter'); 
			$google = get_option('google');
			$youtube = get_option('youtube');
			$instagram = get_option('instagram');
			$linkedin = get_option('linkedin');
			echo "<ul class='list-inline list-unstyled' id='redes_sociais'>";
			if (!empty($facebook)) {
				echo "<a target='_blank' href='$facebook'><li><span class='fa fa-facebook-square'></span></li></a>";
			};
			if (!empty($twitter)) {
				echo "<a target='_blank' href='$twitter'><li><span class='fa fa-twitter'></span></li></a>";
			}
			if (!empty($google)) {
				echo "<li><a target='_blank' href='$google'><span class='fa fa-google'></span></a></li>";
			}
			if (!empty($instagram)) {
				echo "<a target='_blank' href='$instagram'><li><span class='fa fa-instagram'></span></li></a>";
			}
			if (!empty($linkedin)) {
				echo "<a target='_blank' href='$linkedin'><li><span class='fa fa-linkedin'></span></li></a>";
			}
			if (!empty($youtube)) {
				echo "<a target='_blank' href='$youtube'><li><span class='fa fa-youtube'></span></li></a>";
			}
			echo "</ul>";
		}

		class social_widget extends WP_Widget {

	    // constructor
			function social_widget() {
				parent::WP_Widget(false, $name = __('Links Redes Sociais', 'wp_widget_plugin') );
			}

	    // display widget
			function widget($args, $instance) {
				extract( $args );

				echo $before_widget; ?>

				<div class="widget-text social_widget_box">
					<?php redes_sociais(); ?>
				</div>

				<?php echo $after_widget;
			}
		}

/* Google Tag Manager
/---------------------------------------------------------------------------- */

function wp_tag_manager_submenu_page() { ?>
<div class="wrap">
	<div id="icon-tools" class="icon32"></div>
	<form method="post" action="options.php" style="float:left; width:70%">
		<?php settings_fields( 'settings-group-tag' ); ?>
		<fieldset style=" width:100%; padding:20px; margin:30px;">
			<h2 style="font-weight:bold; font-size:24px;">Google Tag Manager</h2>

			<div class="clear" style="margin-bottom:20px">
				<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Script Tag Manager HEAD</label>
				<textarea style="width:100%; background:#fff; border-radius:5px; height:100px" name="tag_head" type="text"><?php echo get_option('tag_head'); ?></textarea>
			</div>

			<div class="clear" style="margin-bottom:20px">
				<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Script Tag Manager BODY</label>
				<textarea  style="width:100%; background:#fff; border-radius:5px; height:100px" name="tag_body"><?php echo get_option('tag_body'); ?></textarea>
			</div>

			<div class="clear" style="margin-bottom:20px">
				<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">API Google Maps</label>
				<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="api_maps" type="text" value="<?php echo get_option('api_maps'); ?>" />

			</div>

			<div style="clear:both"></div>
			<p class="submit">
				<input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />
			</p>

		</fieldset>		

	</form>
</div>
<?php
}

?>