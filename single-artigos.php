<?php 
get_header(); 
$image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()));
?>
<section id="sec_single_noticia_topo" style="background-image: url('<?php echo $image; ?>');">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<h1><?php the_title(); ?></h1>
				<ul class="box_info_noticia">
					<li><?php the_field('data_artigo'); ?></li>
					<li><?= gt_get_post_view(); ?></li>
					<li>0 Compartilhamentos</li>
				</ul>
			</div>
		</div>
	</div>
</section>
<section id="sec_single_noticia_conteudo">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-9">
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
						<p>
							<?php the_content($PostPrev->ID); ?>
						</p>
					</div>
				<?php endwhile; endif; ?>
			</div>
			<div class="col-12 col-md-3">
				<div id="box_sociais_sidebar">
					<div class="box_topo_redes_sidebar">
						<div></div>
						<span>
							<h3>SIGAM-NOS</h3>
							<p>NAS REDES SOCIAS</p>	
						</span>
					</div>
					<ul>
						<li class="sidebar_f"><a target="_blank" href="<?php echo get_option('facebook') ?>"><i class="fab fa-facebook-f"></i></a></li>
						<li class="sidebar_i"><a target="_blank" href="<?php echo get_option('instagram') ?>"><i class="fab fa-instagram"></i></a></li>
						<li class="sidebar_y"><a target="_blank" href="<?php echo get_option('youtube') ?>"><i class="fab fa-youtube"></i></a></li>
					</ul>
				</div>
				<?php dynamic_sidebar('sidebar-noticia'); ?>
			</div>
		</div>
	</div>	
</section>
<section id="sec_single_noticia_prev_next">
	<?php 
	$PostPrev = get_previous_post();
	$PostNext = get_next_post();
	?>
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-9">
				<div class="row">
					<div class="col-12 col-md-6 box_prev">
						<a href="<?php the_permalink($PostPrev->ID); ?>"><i class="fas fa-angle-double-left"></i> Anterior</a>
						<?php if (get_previous_post()): ?>
							<a href="<?php the_permalink($PostPrev->ID); ?>">
								<h3><?php the_title($PostPrev->ID); ?></h3>
							</a>
							<p><?php the_field('breve_resumo',$PostNext->ID); ?></p>
						<?php endif ?>
					</div>
					<div class="col-12 col-md-6">
						<?php if (get_next_post()): ?>
							<div class="w-100 text-right">
								<a href="<?php the_permalink($PostNext->ID); ?>">Próximo <i class="fas fa-angle-double-right"></i></a>	
							</div>
							<a href="<?php the_permalink($PostNext->ID); ?>">
								<h3><?php the_title($PostNext->ID); ?></h3>
							</a>
							<p><?php the_field('breve_resumo',$PostNext->ID); ?></p>
						<?php endif ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php echo get_template_part('inc/template', 'form'); ?>
    
<?php get_footer(); ?>


