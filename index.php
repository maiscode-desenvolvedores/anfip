<?php
	get_header(); 



?> 
<section id="carousel_noticias" class="carousel slide sec_banner" data-ride="carousel">
  <div class="carousel-inner">
    <?php
    $posts_destaque = array();
    if (have_posts()) :
    	while (have_posts()): the_post(); 
    		$especial = get_field('destaque');
    		if ($especial == true) :
	   			array_push($posts_destaque, $post);
	   		endif;
    	endwhile;
    endif;
    wp_reset_postdata();
    if(isset($posts_destaque)):
      global $post; 
      $cont = 0;
      $show = 'active';
      foreach ($posts_destaque as $post):
        $image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()));
        	$categories = get_the_category();
        ?>
        	<div class="carousel-item <?php echo $show; ?>">
        		<div class="box_banner_noticias" style="background-image: url('<?php echo $image; ?>');"></div>
        		<div class="box_conteudo_banner_noticias">
        			<div class="container">
        				<div class="row">
        					<div class="col-12">
        							
        					<?php  
								if ( ! empty( $categories ) ) {
							?>
								 <span><?php echo esc_html( $categories[0]->name ); ?></span>	   
						    <?php  
								}
							 ?>
        					</div>
        					<div class="col-12">
        						<div class="box_titulo_topo">
	        						<a href="<?php the_permalink(); ?>">
	        							<h2><?php the_title()?></h2>
		    								<ul class="box_info_noticia">
		    									<li><?php the_time( 'j \d\e  F, Y' ); ?></li>
		    									<li><?php echo gt_get_post_view(); ?></li>
		    									<li><i class="fas fa-share-alt"></i><?php echo do_shortcode('[wpusb remove_counter="0"]'); ?> Compartilhamento</li>
		    								</ul>
	        						</a> 		
        						</div>
        					</div>
        				</div>
        			</div>
        		</div>
        	</div>	
        <?php
        $cont++;
        $show = '';
      endforeach;
    endif;
    wp_reset_postdata();
    ?>
  </div>
  <ol class="carousel-indicators">
      <?php
      if(isset($posts_destaque)):
      	global $post;
      	$cont = 0;
      	$show = 'active';
        foreach ($posts_destaque as $post) :
	        $image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()));
	        ?>
	         	<li data-target="#carousel_noticias" data-slide-to="<?php echo $cont; ?>" class="<?php echo $show; ?>"></li>	
	        <?php
	        $cont++;
	        $show = '';
        endforeach;
      endif;
      wp_reset_postdata();
      ?>
    </ol> 
</section>
<section id="sec_noticias_ultimas">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h2><?php echo get_option('titulo_primeiro_noticias'); ?></h2>
				<?php if (have_posts()): ?>
					<div class="box_slider_noticias" id="ultimasNoticias">
					<?php while(have_posts()): the_post(); 
						$image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()));
						
						$_resumo = get_the_excerpt();
						$resumo = substr( $_resumo, 0, 225 );
						?>
						<a href="<?php the_permalink(); ?>">
							<div class="box_slide_noticia">
								<img src="<?php echo $image; ?>">
								<div class="box_cont_slide_noticia  text-center">
									
									<h3><?php the_title(); ?></h3>
									<?php if( get_field('breve_resumo') ){ ?>
									    <p><?php the_field('breve_resumo'); ?></p>
									<?php } else{ ?>							
										<p><?php echo $resumo; echo (strlen($resumo) >= 225) ? "..." : ""; ?></p></p>	
									<?php } ?>

								</div>
								<div class="box_tag_noticia">
									<p class="dia_tag_noticia"><?php the_time( 'j' ); ?></p>
									<p class="mes_tag_noticia"><?php the_time( 'F' ); ?></p>							
								</div>								
							</div>
						</a>						
					<?php endwhile; ?>
					</div>
				<?php endif;
				wp_reset_postdata(); ?>
			</div>
		</div>
	</div>
</section>
<?php function slider_ultimaNoticia(){
	?>
	<script type="text/javascript">
		$('#ultimasNoticias').slick({
			slidesToShow: 3,
			slidesToScroll: 3,
			infinite: true,
			speed: 300,
			prevArrow: '<span class="slick-prev"><i class="fas fa-chevron-left"></i></span>',
			nextArrow: '<span class="slick-next"><i class="fas fa-chevron-right"></i></span>',
			responsive: [
			{
				breakpoint: 1024,
				settings: {
					infinite: true,
				}
			},
			{

				breakpoint: 600,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
			]
		});
		
	</script>
	<?php
}
add_action('wp_footer','slider_ultimaNoticia',155); ?>

<?php echo get_template_part('inc/template', 'form'); ?>

<section id="sec_noticias_noticias">
	<div class="container">
		<div class="row box_title_search_noticias">
			<div class="col-12 col-md-8 text-center text-md-left">
				<h2><?php echo get_option('titulo_segundo_noticias'); ?></h2>
			</div>
			<div class="col-12 col-md-4 d-flex justify-content-md-end align-items-center box_searchform_noticia">
				<i class="fas fa-search"></i>
				<form action="<?php bloginfo('siteurl'); ?>" id="searchform_noticia" method="get" class="d-flex"> 
					<div id="search_noticias">

						<!-- <label for="s" class="screen-reader-text mb-0">Pesquisar:</label> -->
						<input type="text" id="s" name="s" value="" placeholder="Buscar" />

						<!-- <input class="btn" type="submit" value="Search" id="searchsubmit" /> -->
					</div>
				</form>
			</div>
		</div>
		<div class="row my-posts">
			<?php if (have_posts()) : ?>
				<?php while (have_posts()) : the_post(); 
					$image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()));
					$_resumo = get_the_excerpt();
					$resumo = substr( $_resumo, 0, 225 );

					if ($image == ''): ?>
						<div class="col-12">
							<a href="<?php the_permalink(); ?>">
								<h3><?php the_title(); ?></h3>
								<?php if( get_field('breve_resumo') ){ ?>
								    <p><?php the_field('breve_resumo'); ?></p>
								<?php } else{ ?>							
									<p><?php echo $resumo; echo (strlen($resumo) >= 225) ? "..." : ""; ?></p></p>	
								<?php } ?>

								<span><?php the_time( 'j \d\e  F, Y' ); ?></span>	
							</a>
							<hr>
						</div>
					<?php else: ?>
						
						<div class="col-12">
							<a href="<?php the_permalink(); ?>">
								<div class="row">
									<div class="col-12 col-md-4 box_img_noticia">
										<img src="<?php echo $image; ?>">
									</div>
									<div class="col-12 col-md-8 box_cont_noticia">
										<h3><?php the_title(); ?></h3>
										<?php if( get_field('breve_resumo') ){ ?>
										    <p><?php the_field('breve_resumo'); ?></p>
										<?php } else{ ?>							
											<p><?php echo $resumo; ?></p>	
										<?php } ?>
										<span><?php the_time( 'j \d\e  F, Y' ); ?></span>	
									</div>	
								</div>
							</a>
							<hr>
						</div>
					<?php endif; 
				endwhile;
				//paginacao(); ?>
			<?php else : ?>
				<h2>Nada Encontrado</h2>
			<?php endif; 
			wp_reset_postdata();?>	
		</div>
		<!-- Carrega Mais -->
        <?php get_template_part('inc/load', 'more'); ?>	
	</div>
</section>

<?php get_footer(); ?>

