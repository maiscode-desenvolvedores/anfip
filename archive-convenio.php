<?php get_header();?>


<?php 
 /*

<section id="sec_convenio_topo" style="background-image: url('<?php echo get_option('banner_convenio') ?>');">
	<div class="container">
		<div class="row">
			<div class="col-12 offset-md-4 col-md-6 text-right">
				<h1><?php echo get_option('titulo_convenio_topo'); ?></h1>
				<p><?php echo get_option('subtitulo_convenio'); ?></p>
			</div>
		</div>
	</div>
</section>


 */

 ?>

<section id="sec_convenio_convenios">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h2><?php echo get_option('titulo_convenios'); ?></h2>	
			</div>
		</div>
		<div class="row justify-content-center">
			<?php if (have_posts()): ?>
				<?php while(have_posts()): the_post(); $image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()));?>
					<div class="box_convenio" data-titulo="<?php the_title(); ?>" data-conteudo="<?php the_field('conteudo_convenio'); ?>" data-target="#modal_convenio" data-toggle="modal">  
						<img src="<?php echo $image; ?>">
						<h3><?php the_title(); ?></h3>
					</div>
				<?php endwhile ?>
			<?php else : ?>
				<h2>Nenhum convênio encontrado!</h2>
			<?php endif ?>
		</div>
	</div>
</section>
<section id="sec_convenio_associado" style="background-image: url('<?php echo get_option('banner_associado'); ?>');">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-5">
				<h2><?php echo get_option('titulo_associados') ?></h2>
				<p><?php echo get_option('texto_associados'); ?></p>
				<a href="<?php echo get_home_url(); ?>/associe-se-a-anfip"><span><?php echo get_option('texto_btn_associado'); ?></span></a>
			</div>
		</div>
	</div>
</section>
<div class="modal fade" id="modal_convenio" tabindex="-1" role="dialog" aria-labelledby="modal_convenioTitle" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	  	<div class="modal-body">
	  		<h5 class="modal-title" id="titulo_modal_convenio"></h5>
	    	<p id="conteudo_modal_convenio"></p>
	  	</div>
	</div>
</div>


<?php function modal_convenio(){ ?>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script type="text/javascript">
		$('.box_convenio').click(function(){
			var titulo = $(this).data('titulo');
			var conteudo = $(this).data('conteudo');
			//console.log(titulo);
			$('#titulo_modal_convenio').html(titulo);
			$('#conteudo_modal_convenio').html(conteudo);
		});
	</script>
<?php } 

add_action('wp_footer','modal_convenio',300);?>

<?php get_footer(); ?>