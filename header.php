<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<head>
    <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
    <meta name="robots" content="follow,all" />
    <meta http-equiv="Content-Language" content="pt-br" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/favicon.png" type="image/png"/>
    <link href="https://fonts.googleapis.com/css2?family=Libre+Franklin:wght@100&display=swap" rel="stylesheet">
    

    <script src="https://kit.fontawesome.com/897255f20e.js" crossorigin="anonymous"></script>

    <?php wp_head(); ?>
    <?php echo get_option('tag_head'); ?>
</head>
<body <?php body_class(); ?>>
    <?php echo get_option('tag_body'); ?>

<header>
    <!-- Desktop -->
    <div class="bg_header js-transparente d-none d-lg-block">
        <div class="container">                
             <div class="row">

                    <button class="navbar-toggler btn_header_menu js-filtro col-md-1" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                        <i class="fas fa-bars"></i>
                    </button>


                 

                   <div class="col-md-6">

                        <a class="logomarca" href="<?php echo get_home_url(); ?>">
                            <img class="js-filtro " src="<?php echo get_option('header_logo'); ?>" alt="<?php bloginfo('name'); ?>" class="align-middle" alt="">
                        </a>    

                  </div>

                    <div class="col-md-2 btn_midia">

                            <ul class="box_social_header ">
                                <li>   
                                    <button class="navbar-toggler btn_pesquisa_menu" type="button" data-toggle="collapse" data-target="#search" aria-controls="search" aria-expanded="false" aria-label="Toggle navigation">
                                     <i class="pesquisa_icone fas fa-search js-filtro"></i>
                                     </button>
                                </li>
                                <li><a target="_blank" href="<?php echo get_option('facebook'); ?>"><i class="fab fa-facebook-f js-filtro"></i></a></li>
                                <li><a target="_blank" href="<?php echo get_option('instagram'); ?>"><i class="fab fa-instagram js-filtro"></i></a></li>
                                <li><a target="_blank" href="<?php echo get_option('youtube'); ?>"><i class="fab fa-youtube js-filtro"></i></a></li>
                            </ul>
                        
                    </div>

                    <div class="button-user col-md-3 botoes">

                        <?php if (!is_user_logged_in()): ?>
                            <a  class="align-self-center" href="<?php echo get_home_url(); ?>/associe-se-a-anfip">
                                <span class="btn btn_associe_header js-botao">Associe-se</span>
                            </a>    
                            <button class="btn btn-primary btn_login_header" type="button" data-toggle="collapse" data-target="#login_restrito">
                                <span class="um-icon-ios-person-outline"></span>
                            </button>
                        <?php else:?>  
                            <a  class="align-self-center" href="<?php echo get_home_url(); ?>/logout">
                                <span class="btn btn_associe_header js-botao">Sair<i class="fas fa-sign-out-alt"></i></span>
                            </a>
                        <?php endif; ?>
        
                        
                    </div>


            </div>

            <div class="col-md-4 offset-md-8">
              <?php get_search_form(); ?>
                
            </div>

         </div>


     </div>


    </div>

    <div class="row">
        <div class="container">
            <div class="col-12">

                <div class="collapse navbar-collapse box_menu_header" id="navbarNavAltMarkup">
                    <div class="navbar-nav">
                        <?php
                        $defaults = array(
                            'theme_location'  => 'header-menu',
                            'container'       => false,
                            'menu_class'      => 'list-inline list-unstyled box-menu-header ',
                            'menu_id'         => 'menu',
                            'echo'            => true
                        );
                        wp_nav_menu($defaults);
                        ?>
                    </div>                          
                </div>  



                <div class="box_login_menu_header col-md-4 login_desktop collapse" id="login_restrito">
                           <!-- Formulário menu --> 
                        <?php if (!is_user_logged_in()): ?>                                 
                            <?php echo do_shortcode('[ultimatemember form_id="519"]'); ?> 

                            <div class="links-forms">

                                <div class="um-right um-half">
                                    <a href="<?php echo get_home_url(); ?>/register/" class="um-alt" data-toggle="modal" data-target="#modalCadastro">
                                        Cadastre-se         
                                    </a>
                                </div>

                                <div class="um-col-alt-b">
                                    <a href="<?php echo get_home_url(); ?>/password-reset/" class="um-link-alt" data-toggle="modal" data-target="#modalRecuperaSenha">
                                        Esqueceu sua senha?    
                                     </a>
                                </div>
                                
                            </div>

                        <?php else:?>
                            <h2 class="bem-vindo">Bem vindo,</br><strong><?php echo um_user('display_name');?></strong></h2>
                            <a class="botao-sair" href="<?php echo get_home_url(); ?>/logout">Sair<i class="fas fa-sign-out-alt"></i></a>
                        <?php endif; ?>   

                </div>
                
            </div>
                    
        </div>
    </div>

    
    <!-- Mobile -->




    <nav class="navbar navbar-expand-lg navbar-light bg-light d-lg-none bg_header js-transparente">
     
        <button class="navbar-toggler btn_header_menu js-filtro" type="button" data-toggle="collapse" data-target="#menuMobile" aria-controls="menuMobile" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fas fa-bars"></i>
        </button>

        <div class="collapse navbar-collapse box_menu_header" id="menuMobile"> 
            <div class="navbar-nav">

                <?php include(__DIR__.'/searchform.php'); ?>
                <?php
                $defaults = array(
                    'theme_location'  => 'header-menu',
                    'container'       => false,
                    'menu_class'      => 'list-inline list-unstyled box-menu-header ',
                    'menu_id'         => 'menu',
                    'echo'            => true
                );
                wp_nav_menu($defaults);
                ?>
                <ul class="box_social_header d-flex align-self-center">
                    <li><a target="_blank" href="<?php echo get_option('facebook'); ?>"><i class="fab fa-facebook-f"></i></a></li>
                    <li><a target="_blank" href="<?php echo get_option('instagram'); ?>"><i class="fab fa-instagram"></i></a></li>
                    <li><a target="_blank" href="<?php echo get_option('youtube'); ?>"><i class="fab fa-youtube"></i></a></li>
                </ul>
                <a  class="align-self-center" href="<?php echo get_home_url(); ?>/associe-se-a-anfip"><span class="btn btn_associe_header">Associe-se</span></a> 
                <button class="btn btn-primary btn_login_header" type="button" data-toggle="collapse" data-target="#login_restrito">
                    <span>Acesso Restrito</span>
                </button>     

          </div>

            <div class="box_login_menu_header text-center collapse" id="login_restrito">
            <!-- Formulário menu --> 
            <?php if (!is_user_logged_in()): ?>                                 
              <?php echo do_shortcode('[ultimatemember form_id="519"]'); ?> 

                    <div class="links-forms">

                        <div class="um-right um-half">
                            <a href="<?php echo get_home_url(); ?>/register/" class="um-alt" data-toggle="modal" data-target="#modalCadastro">
                                Cadastre-se         
                            </a>
                        </div>

                        <div class="um-col-alt-b">
                            <a href="<?php echo get_home_url(); ?>/password-reset/" class="um-link-alt" data-toggle="modal" data-target="#modalRecuperaSenha">
                                Esqueceu sua senha?    
                             </a>
                        </div>
                        
                    </div>
                    
            <?php else:?>
              <h2 class="bem-vindo">Bem vindo,</br><strong><?php echo um_user('display_name');?></strong></h2>
              <a class="botao-sair" href="<?php echo get_home_url(); ?>/logout">Sair<i class="fas fa-sign-out-alt"></i></a>
           <?php endif; ?>   
            </div> 
  
       </div>


        <a class="align-self-center d-flex" href="<?php echo get_home_url(); ?>">
            <img src="<?php echo get_option('header_logo'); ?>" alt="<?php bloginfo('name'); ?>" class="js-filtro align-middle logomarca" alt="">
        </a>    
    </nav> 


    <?php
    function collapseMenu(){
    ?>
    <script type="text/javascript">
        $('.sub-menu').addClass('collapse');
        $('.menu-item-has-children').children('a').append('<i class="fas fa-chevron-down"></i>');
        $('.menu-item-has-children').click(function(){

            var submenu = $(this).find('.sub-menu').hasClass('show');
            if (submenu == false) {
                $(this).find('.sub-menu').addClass('show');
                $(this).css('border','none');
                $(this).find('.sub-menu').css('border-top','0.5px solid rgba(255,255,255,.5)');
            }else{
                $(this).find('.sub-menu').removeClass('show');
                $(this).css('border-bottom','0.5px solid rgba(255,255,255,.5)');
            }
            
        
        });

        // $('.btn_header_menu').click(function(){
        //     if ($(this).find('i').hasClass("fa-bars")) { //se tem olho aberto
        //         $(this).find('i').removeClass("fa-bars"); //remove classe olho aberto
        //         $(this).find('i').addClass("fa-times-circle"); //coloca classe olho fechado
        //     } else { //senão
        //         $(this).find('i').removeClass("fa-times-circle"); //remove classe olho fechado
        //         $(this).find('i').addClass("fa-bars"); //coloca classe olho aberto
        //     }
        // });     
         

        // Evitar redirecionamento e fechar menu ao abrir modal
        $(document).ready(function() { 
            $(".um-button.um-alt").click(function(event) { 
                event.preventDefault();
                document.querySelector('.box_menu_header').classList.remove('show');
            }); 
        });

        $(document).ready(function() { 
            $(".um-link-alt").click(function(event) { 
                event.preventDefault();
                document.querySelector('.box_menu_header').classList.remove('show');
            }); 
        });



        //Muda nome do campo enviar
        document.querySelector('.box_login #um-submit-btn').value = '🡪';  
        
        //Verifica se tem erro no cadastro 
        if ($("p.err").length) { 
            document.querySelector('.um-button.um-alt').click();
        } 
    
    </script>
    <?php
    }
    add_action('wp_footer','collapseMenu',300);?>
    
    <?php /* if (!is_user_logged_in()): ?>

        <div class="box_login js-mostrar">

            <?php echo do_shortcode('[ultimatemember form_id="519"]'); ?>                
        </div>

    <?php endif;
    $tipo_single = get_field('tipo_de_noticia');

        */    
    ?>
    
   
</header><!--fim header-->

    </button>

    <div class="modal fade " id="modalRecuperaSenha" tabindex="-1" role="dialog" aria-labelledby="modalEsqueceuSenhaLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="box_modal_login">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                      <span aria-hidden="true">&times;</span>
                  </button>
                  <h2><?php echo get_option('titulo_form_recupera_senha'); ?></h2>
                  <p><?php echo get_option('texto_form_recupera_senha'); ?></p>
                  
                  <?php echo do_shortcode('[ultimatemember_password]'); ?>
                </div>
                
            </div>
        </div>       
    </div>

    <div class="modal fade " id="modalCadastro" tabindex="-1" role="dialog" aria-labelledby="modalCadastroLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="box_modal_login">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                  <span aria-hidden="true">&times;</span>
                </button>
                <h2><?php echo get_option('titulo_form_cadastro'); ?></h2>
                <p><?php echo get_option('texto_form_cadastro') ?></p>
                <div id="form_cadastro" class="text-center">
                    <?php echo do_shortcode('[ultimatemember form_id="518"]'); ?>
                </div>    
                </div>
                
            </div>
        </div>
    </div>