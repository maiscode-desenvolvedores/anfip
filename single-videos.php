<?php get_header(); ?>
<section id="sec_single_videos">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center" id="post-<?php the_ID(); ?>">
				<h1><?php the_title(); ?></h1>
			</div>
		</div>
		<div class="row">
			<div class="col-12 offset-md-3 col-md-6 text-center">
				<?php if (have_rows('videos')):
					$cont = 1;
					while(have_rows('videos')) : the_row(); ?>
						
						<h4>Parte <?php echo $cont; ?></h4>
						<?php the_sub_field('video'); 
						$cont++;
					endwhile;
				endif; ?>
			</div>
		</div>
	</div>	
</section>
<?php get_footer(); ?>