<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<head>
    <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
    <meta name="robots" content="follow,all" />
    <meta http-equiv="Content-Language" content="pt-br" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/favicon.png" type="image/png"/>
    <link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
      <link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/fancybox/jquery.fancybox-thumbs.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />

    <script src="https://kit.fontawesome.com/897255f20e.js" crossorigin="anonymous"></script>
    <?php wp_head(); ?>
    <?php echo get_option('tag_head'); ?>
</head>
<body <?php body_class(); ?>>
    <?php echo get_option('tag_body'); ?>
    <header>

        <!-- Desktop -->
        <div class="bg_header_single d-none d-lg-block">
            <div class="container header-desktop">
                <!-- <div class="row"> -->
                    <div class="col-12 col-md-1">
                        <button class="navbar-toggler btn_header_menu" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                            <i class="fas fa-bars"></i>
                        </button>
                        <div class="collapse navbar-collapse box_menu_header" id="navbarNavAltMarkup">
                            <div class="navbar-nav ">
                                <?php
                                $defaults = array(
                                    'theme_location'  => 'header-menu',
                                    'container'       => false,
                                    'menu_class'      => 'list-inline list-unstyled box-menu-header ',
                                    'menu_id'         => 'menu',
                                    'echo'            => true
                                );
                                wp_nav_menu($defaults);
                                ?>

                            </div>
                            <form class="box_login_menu_header text-center">
                                <input type="text" id="login" name="login" placeholder="Siape">
                                <input type="text" name="password" placeholder="Senha">
                                <div class="d-flex ">
                                    <a href="" class="text-center">Esqueceu sua senha?</a>
                                    <a href="" class="text-center">Cadastre-se</a>
                                </div>
                                <button class="btn btn_login_m_h" type="submit">
                                    Entrar
                                </button>
                            </form>
                        </div>    
                    </div>
                    <div class="col-12 offset-md-3 col-md-8 d-flex ">
                        <a class="align-self-center" href="<?php echo get_home_url(); ?>">
                            <img src="<?php echo get_option('header_logo'); ?>" alt="<?php bloginfo('name'); ?>" class="align-middle" alt="">
                        </a>    
                        <?php include(__DIR__.'/searchform.php'); ?>
                        <ul class="box_social_header d-flex align-self-center">
                            <li><a href="<?php echo get_option('facebook'); ?>"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="<?php echo get_option('instagram'); ?>"><i class="fab fa-instagram"></i></a></li>
                            <li><a href="<?php echo get_option('youtube'); ?>"><i class="fab fa-youtube"></i></a></li>
                        </ul>
                        <a  class="align-self-center" href="<?php echo get_home_url(); ?>/associe-se-a-anfip"><span class="btn btn_associe_header">Associe-se</span></a>    
                    </div>  
                <!-- </div> -->
            </div>
        </div>
        
        
        <!-- Mobile -->

        <nav class="navbar navbar-expand-lg navbar-light bg-light d-lg-none bg_header">
                <button class="navbar-toggler btn_header_menu" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fas fa-bars"></i>
                </button>
                <div class="collapse navbar-collapse box_menu_header" id="navbarNavAltMarkup">
                    <div class="navbar-nav ">
                        <?php include(__DIR__.'/searchform.php'); ?>
                        <?php
                        $defaults = array(
                            'theme_location'  => 'header-menu',
                            'container'       => false,
                            'menu_class'      => 'list-inline list-unstyled box-menu-header ',
                            'menu_id'         => 'menu',
                            'echo'            => true
                        );
                        wp_nav_menu($defaults);
                        ?>
                        <ul class="box_social_header d-flex align-self-center">
                            <li><a href="<?php echo get_option('facebook'); ?>"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="<?php echo get_option('instagram'); ?>"><i class="fab fa-instagram"></i></a></li>
                            <li><a href="<?php echo get_option('youtube'); ?>"><i class="fab fa-youtube"></i></a></li>
                        </ul>
                        <a  class="align-self-center" href="<?php echo get_home_url(); ?>/associe-se-a-anfip"><span class="btn btn_associe_header">Associe-se</span></a>  
                    </div>
                    <form class="box_login_menu_header text-center">
                        <input type="text" id="login" name="login" placeholder="Siape">
                        <input type="text" name="password" placeholder="Senha">
                        <div class="d-flex ">
                            <a href="" class="text-center">Esqueceu sua senha?</a>
                            <a href="" class="text-center">Cadastre-se</a>
                        </div>
                        <button class="btn btn_login_m_h" type="submit">
                            Entrar
                        </button>
                    </form>
                </div>    
                <a class="align-self-center" href="<?php echo get_home_url(); ?>">
                    <img src="<?php echo get_option('header_logo'); ?>" alt="<?php bloginfo('name'); ?>" class="align-middle" alt="">
                </a>    


        </nav>
        
        <?php 
        function collapseMenu(){
        ?>
        <script type="text/javascript">
            $('.sub-menu').addClass('collapse');
            $('.menu-item-has-children').children('a').append('<i class="fas fa-chevron-down"></i>');
            $('.menu-item-has-children').click(function(){

                var submenu = $(this).find('.sub-menu').hasClass('show');
                if (submenu == false) {
                    $(this).find('.sub-menu').addClass('show');
                }else{
                    $(this).find('.sub-menu').removeClass('show');
                }
                //console.log(submenu);
            
            });

            $('.btn_header_menu').click(function(){
                if ($(this).find('i').hasClass("fa-bars")) { //se tem olho aberto
                    $(this).find('i').removeClass("fa-bars"); //remove classe olho aberto
                    $(this).find('i').addClass("fa-times-circle"); //coloca classe olho fechado
                } else { //senão
                    $(this).find('i').removeClass("fa-times-circle"); //remove classe olho fechado
                    $(this).find('i').addClass("fa-bars"); //coloca classe olho aberto
                }
            });
        </script>
        <?php
        }
        add_action('wp_footer','collapseMenu',300);
        ?>
    </header><!--fim header-->
