<?php get_header(); 

// $url_redirect = get_home_url();

// if (!is_user_logged_in()) {
// 	wp_redirect($url_redirect);
// }
?>
<section id="sec_top_patrimoniais" style="background-image: url(<?php echo get_option('banner_patrimoniais'); ?>);">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<h1><?php echo get_option('titulo_patrimoniais'); ?></h1>
			</div>
		</div>
	</div>
</section>
<section id="sec_patrimoniais_patrimoniais">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<ul class="box_list_post my-posts">
				<?php if (have_posts()) : ?>
					<?php while(have_posts()) : the_post(); 
						$image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()));
						?>
						<li class="d-flex">
							<h2><span><?php the_title(); ?></span></h2>	
							<a href="<?php the_field('pdf_patrimonio'); ?>" target="_blank">
								<button>Ver mais</button>	
							</a>
						</li>
						<hr class="hr_list_post">
					<?php endwhile ?>
				<?php else : ?>
					<h2>Nada Encontrado</h2>
				<?php endif; ?>	
				</ul>
			</div>
		</div>
		<!-- Carrega mais -->
        <?php get_template_part('inc/load', 'more'); ?>	
	</div>	
</section>
<?php get_footer();