<?php get_header(); ?>

<section id="sec_top_fotos" style="background-image: url(<?php echo get_option('banner_fotos'); ?>);">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<h1><?php echo get_option('titulo_fotos'); ?></h1>
			</div>
		</div>
	</div>
</section>

<section id="sec_fotos_fotos">
	<div class="container">
		<div class="row my-posts">
			<?php $cont = 1;
      if (have_rows('galeria')) : ?>
				<?php while(have_rows('galeria')) : the_row(); 
					?>

              <div class="col-12 col-md-3">
                  <a data-fancybox="gallery" href="<?php the_sub_field('imagem'); ?>" class="gallery" data-caption="Imagem<?php echo $cont; ?>"> 
                    <div class="box_img_fotos">
                      <img class="arqImgFit" thumbnail="gallery-<?php echo $cont; ?>" src="<?php the_sub_field('imagem'); ?>">
                    </div>      
                  </a>
                </div>

				<?php $cont++;
        endwhile ?>
			<?php else : ?>
				<h2>Nada Encontrado</h2>
			<?php endif; ?>	
		</div>

		<?php /*
	<div class="row">					
			<div class="col-12 box_paginacao text-center">
				<button class="carrega-mais btn btn_vermais" id="element" style=" border: none; background: transparent; color: white;"></button>
				<div id="vejamais" style="display: none">
					<div class="loader"></div>
				</div>
			</div>   
		</div>
		*/ ?>
		
	</div>	
</section> 

 <?php function fancyPage(){ ?>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
    <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
    <script type="text/javascript">
       
       $(document).ready(function(){
        $('[data-fancybox="gallery"]').fancybox({
          thumbs: {
            autoStart: false, // Display thumbnails on opening
            hideOnClose: false, // Hide thumbnail grid when closing animation starts
            axis: "x" // Vertical (y) or horizontal (x) scrolling
          },
         
          transitionEffect: "circular",
          buttons: [
            //"zoom",
            //"share",
            //"slideShow",
            //"fullScreen",
            "download",
            //"thumbs",
            "close"
          ]
        });
      });

    </script>  

<?php }
add_action('wp_footer', 'fancyPage', 450);

get_footer(); ?>