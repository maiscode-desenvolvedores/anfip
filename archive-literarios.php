<?php get_header(); 

// $url_redirect = get_home_url();

// if (!is_user_logged_in()) {
// 	wp_redirect($url_redirect);
// }
?>
<section id="sec_top_literarios" style="background-image: url(<?php echo get_option('banner_literarios'); ?>);">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<h1><?php echo get_option('titulo_literarios'); ?></h1>
			</div>
		</div>
	</div>
</section>
<section id="sec_literarios_literarios">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<ul class="box_list_post my-posts">
				<?php if (have_posts()) : ?> 
					<?php while(have_posts()) : the_post(); 
				?>
						<li class="d-flex">
							<h2><span><?php// the_date('d-m-Y'); ?></span><?php the_title(); ?></h2>
							<a href="<?php the_permalink(); ?>">
								<button>Ver mais</button>	
							</a>
						</li>
						<hr class="hr_list_post">
					<?php endwhile ?>
				<?php else : ?>
					<h2>Nada Encontrado</h2>
				<?php endif; ?>	
				</ul>
			</div>
		</div>
		<!-- Carrega Mais -->
        <?php get_template_part('inc/load', 'more'); ?>
	</div>	
</section>
<?php get_footer();