<?php /* get_header(); ?>
<section id="carousel_noticias" class="carousel slide sec_banner" data-ride="carousel">
  <div class="carousel-inner">
    <?php
    $posts_destaque = array();
    if (have_posts()) :
    	while (have_posts()): the_post(); 
    		$especial = get_field('destaque');
    		if ($especial == true) :
	   			array_push($posts_destaque, $post);
	   		endif;
    	endwhile;
    endif;
    wp_reset_postdata();
    if(isset($posts_destaque)):
      global $post;
      $cont = 0;
      $show = 'active';
      foreach ($posts_destaque as $post):
        $image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()));
        
        ?>
        	<div class="carousel-item <?php echo $show; ?>">
        		<div class="box_banner_noticias" style="background-image: url('<?php echo $image; ?>');"></div>
        		<div class="box_conteudo_banner_noticias">
        			<div class="container">
        				<div class="row">
        					<div class="col-12">
        						<span><?php the_field('tag_noticia'); ?></span>		
        					</div>
        					<div class="col-12">
        						<a href="<?php the_permalink(); ?>">
        							<h2><?php the_title()?><br>
        								<ul class="box_info_noticia">
        									<li><?php the_field('data_noticia'); ?></li>
        									<li><?php echo gt_get_post_view(); ?></li>
        								</ul>
        							</h2>
        						</a> 		
        					</div>
        				</div>
        			</div>
        		</div>
        	</div>	
        <?php
        $cont++;
        $show = '';
      endforeach;
    endif;
    wp_reset_postdata();
    ?>
  </div>
  <ol class="carousel-indicators">
      <?php
      if(isset($posts_destaque)):
      	global $post;
      	$cont = 0;
      	$show = 'active';
        foreach ($posts_destaque as $post) :
	        $image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()));
	        ?>
	         	<li data-target="#carousel_noticias" data-slide-to="<?php echo $cont; ?>" class="<?php echo $show; ?>"></li>	
	        <?php
	        $cont++;
	        $show = '';
        endforeach;
      endif;
      wp_reset_postdata();
      ?>
    </ol> 
</section>
<section id="sec_noticias_ultimas">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h2><?php the_field('titulo_ultimas_noticias',9); ?></h2>
				<?php if (have_posts()): ?>
					<div class="box_slider_noticias" id="ultimasNoticias">
					<?php while(have_posts()): the_post(); 
						$image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()));
						

						?>
						<a href="<?php the_permalink(); ?>">
							<div class="box_slide_noticia">
								<img src="<?php echo $image; ?>">
								<div class="box_cont_slide_noticia  text-center">
									
										<h3><?php the_title(); ?></h3>
										
									<p><?php the_field('breve_resumo'); ?></p>	
								</div>
								<div class="box_tag_noticia">
									<p class="dia_tag_noticia"><?php the_field('dia_noticia'); ?></p>
									<p class="mes_tag_noticia"><?php the_field('mes_noticia'); ?></p>							
								</div>
							</div>
						</a>						
					<?php endwhile; ?>
					</div>
				<?php endif;
				wp_reset_postdata(); ?>
			</div>
		</div>
	</div>
</section>
<?php function slider_ultimaNoticia(){
	?>
	<script type="text/javascript">
		$('#ultimasNoticias').slick({
			slidesToShow: 3,
			slidesToScroll: 3,
			infinite: true,
			speed: 300,
			prevArrow: '<span class="slick-prev"><i class="fas fa-chevron-left"></i></span>',
			nextArrow: '<span class="slick-next"><i class="fas fa-chevron-right"></i></span>',
			responsive: [
			{
				breakpoint: 1024,
				settings: {
					infinite: true,
				}
			},
			{

				breakpoint: 600,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
			]
		});
		
	</script>
	<?php
}
add_action('wp_footer','slider_ultimaNoticia',155); ?>
<section id="sec_noticias_increvase">
	<div class="container">
		<div class="row box_inscrever_se" style="background-image: url('<?php echo get_option('imagem_fundo'); ?>');">
			<div class="col-12 col-md-3">
				<span class="box_texto_inscrever_se"><?php echo get_option('texto_form'); ?></span>
			</div>
			<div class="col-12 col-md-9 align-self-end box_form">
				<?php echo do_shortcode('[contact-form-7 id="146" title="Contact form 1"]'); ?>	
			</div>
		</div>	
	</div>
</section>
<section id="sec_noticias_noticias">
	<div class="container">
		<div class="row box_title_search_noticias">
			<div class="col-12 col-md-8 text-center text-md-left">
				<h2><?php the_field('titulo_noticias',9); ?></h2>
			</div>
			<div class="col-12 col-md-4 d-flex justify-content-md-end align-items-center box_searchform_noticia">
				<i class="fas fa-search"></i>
				<form action="<?php bloginfo('siteurl'); ?>" id="searchform_noticia" method="get" class="d-flex"> 
					<div id="search_noticias">

						<!-- <label for="s" class="screen-reader-text mb-0">Pesquisar:</label> -->
						<input type="text" id="s" name="s" value="" placeholder="Buscar" />

						<!-- <input class="btn" type="submit" value="Search" id="searchsubmit" /> -->
					</div>
				</form>
			</div>
		</div>
		<div class="row my-posts">
			<?php if (have_posts()) : ?>
				<?php while (have_posts()) : the_post(); 
					$image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()));
					if ($image == ''): ?>
						<div class="col-12">
							<a href="<?php the_permalink(); ?>">
								<h3><?php the_title(); ?></h3>
								<p><?php the_field('breve_resumo'); ?></p>
								<span><?php the_field('data_noticia'); ?></span>	
							</a>
							<hr>
						</div>
					<?php else: ?>
						<div class="col-12">
							<a href="<?php the_permalink(); ?>">
								<div class="row">
									<div class="col-12 col-md-4 box_img_noticia">
										<img src="<?php echo $image; ?>">
									</div>
									<div class="col-12 col-md-8 box_cont_noticia">
										<h3><?php the_title(); ?></h3>
										<p><?php the_field('breve_resumo'); ?></p>
										<span><?php the_field('data_noticia'); ?></span>	
									</div>	
								</div>
							</a>
							<hr>
						</div>
					<?php endif; 
				endwhile;
				//paginacao(); ?>
			<?php else : ?>
				<h2>Nada Encontrado</h2>
			<?php endif; 
			wp_reset_postdata();?>	
		</div>
		<!-- <div class="row my-posts"></div> -->
		<div class="row">					
			<div class="col-12 box_paginacao text-center">
				<button class="carrega-mais btn btn_vermais" id="element" style=" border: none; background: transparent; color: white;"></button>
				<div id="vejamais" style="display: none">
					<div class="loader"></div>
				</div>
			</div>   
		</div>
	</div>
</section>

<?php 
function ajax_mais(){

    $page_num = 1;// get_option( 'posts_per_page' ); 
    $postType = get_post_type();

    ?>

    <script type="text/javascript">

        var ajaxurl = "<?php echo admin_url( 'admin-ajax.php' ); ?>";
        var controleView = false;
        var page = 1;

        jQuery(function($) {
            $(window).scroll(function() {
                var top_of_element = $("#element").offset().top;
                var bottom_of_element = $("#element").offset().top + $("#element").outerHeight();
                var bottom_of_screen = $(window).scrollTop() + $(window).innerHeight();
                var top_of_screen = $(window).scrollTop();

                if ((bottom_of_screen > top_of_element) && (top_of_screen < bottom_of_element)){
                        // the element is visible, do something
                        if (controleView == false) {
                            testeMais();
                            controleView = true;
                        }
                    } else {
                        // the element is not visible, do something else
                    }
                });
            $('body').on('click', '.carrega-mais', function() {
                testeMais();
            });
            function testeMais(){
                $('#vejamais').show();
                $('.carrega-mais').hide();

                var data = {
                    'action': 'load_posts_by_ajax',
                    'pagina': page,
                    'posts_per_page': '<?php echo $page_num; ?>',
                    'post_type': '<?php echo $postType; ?>',
                    'security': '<?php echo wp_create_nonce("load_more_posts"); ?>'
                };
                //console.log(data);

                setTimeout(function(){
                    $.post(ajaxurl, data, function(response) {
                    	//console.log(response);
                        if(response != '') { 
                            $('.my-posts').append(response);
                            page++;
 
                            $('#vejamais').hide();
                            $('.carrega-mais').show();
                            controleView = false;
                        } else {
                            $('#vejamais').hide();
                            $('.carrega-mais').hide();
                            controleView = "finaliza";
                        }
                    });
                }, 1000);
            }
        });
    </script>
<?php  } 
add_action('wp_footer', 'ajax_mais', 500);

get_footer(); */?>

