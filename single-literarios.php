<?php get_header(); 

?>
<section id="sec_single_literarios">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center" id="post-<?php the_ID(); ?>">
				<h1><?php the_title(); ?></h1>
			</div>

		</div>
		<div class="row">
			<div class="col-12 ">

				<?php if (have_posts()) : the_post(); ?>
					 <?php the_content(); ?>
				<?php endif; ?>			
			</div>
		</div>
	</div>	
</section>
<?php get_footer(); ?>