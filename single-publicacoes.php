<?php get_header(); ?>
<section id="sec_single_publicações">
	<div class="container">
		<div class="row">
			<div class="col-12 offset-md-4 col-md-4 text-center">
				<div <?php post_class(); ?> id="post-<?php the_ID(); ?>">
					<h1><?php the_title(); ?></h1>
					<?php if (has_post_thumbnail()): 
						$image = wp_get_attachment_url( get_post_thumbnail_id($post->ID));
						?>
						<img src="<?php echo $image; ?>">
					<?php endif ?>
					<p><?php the_field('legenda_publicacao'); ?></p>
					<a href="<?php the_field('pdf_publicacao') ?>" download><?php echo get_option('txt_btn_pub_download'); ?></a>
				</div>
			</div>
		</div>
	</div>	
</section>
<?php get_footer(); ?>