<?php get_header(); ?>
<section id="sec_top_artigos" style="background-image: url(<?php echo get_option('banner_artigos'); ?>);">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<h1><?php echo get_option('titulo_artigos'); ?></h1>
			</div>
		</div>
	</div>
</section>
<section id="sec_artigos_artigos">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<ul class="box_list_post my-posts">
				<?php if (have_posts()) : ?>
					<?php while(have_posts()) : the_post(); 
						$image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()));
						?>
						<li class="d-flex">
							<h2><span><?php the_date('d-m-Y'); ?></span> - <?php the_title(); ?></h2>
							<a href="<?php the_permalink(); ?>">
								<button><?php echo get_option('texto_btn_artigo'); ?></button>	
							</a>
						</li>
						<hr class="hr_list_post">
					<?php endwhile ?>
				<?php else : ?>
					<h2>Nada Encontrado</h2>
				<?php endif; ?>	
				</ul>
			</div>
		</div>	
        <!-- Carrega mais -->
        <?php get_template_part('inc/load', 'more'); ?>	
	</div>	
</section>

<?php
get_footer();
