<?php
/**
* Template Name: Página de Contato
*
* @package WordPress
* @author Mais Code Tecnologia
* @since First Version
*/
get_header(); ?>
<section id="sec_contato_topo" style="background-image: url('<?php echo get_option('banner_contato'); ?>');">
  <div class="container">
    <div class="row">
      <div class="col-12 text-center">
        <h1><?php echo get_option('titulo_contato'); ?></h1>
        <p><?php echo get_option('sub_titulo_contato'); ?></p>
      </div>
    </div>
  </div>
</section>
<section id="sec_contato">
  <div class="container">
    <div class="row">
      <div class="col-12 col-md-5">
        <?php echo do_shortcode('[contact-form-7 id="160" title="Fale conosco"]'); ?>      
      </div>
      <div class="col-12 col-md-7">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3751.117251800546!2d-43.939052585423106!3d-19.91946324312504!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xa699fb23d36e6d%3A0x69570d6bf2711926!2sRua%20dos%20Carij%C3%B3s%2C%20150%20-%207%C2%B0%20andar%20-%20Centro%2C%20Belo%20Horizonte%20-%20MG%2C%2030120-060!5e0!3m2!1spt-BR!2sbr!4v1583263862876!5m2!1spt-BR!2sbr" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
        <?php //the_field('mapa'); ?>
      </div>
    </div>
  </div>      
</section>
<?php get_footer(); ?>