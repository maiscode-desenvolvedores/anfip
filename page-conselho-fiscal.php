<?php
/**
* Template Name: Página Conselho Fiscal
*
* @package WordPress
* @author Mais Code Tecnologia
* @since First Version
*/
get_header(); 
$image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()));

$conselho_executivo = array();
$conselho_fiscal = array();

$args = array(        
	'post_type' => 'conselho',
	'posts_per_page' => -1
  );

$loop = new WP_Query( $args );

if ($loop->have_posts()) :
	while($loop->have_posts()) : $loop->the_post(); 
		$tipo_conselho = get_field('tipo_conselho');
		if ($tipo_conselho == 'Executivo') {
			array_push($conselho_executivo, $post);
		}else if($tipo_conselho == 'Fiscal'){
			array_push($conselho_fiscal, $post);
		}
	endwhile;
	
else : ?>
	<h2>Nada Encontrado</h2>
<?php endif; ?>


<section id="sec_top_conselho" style="background-image: url(<?php echo get_option('banner_conselho'); ?>);">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<h1><?php echo get_option('titulo_conselho'); ?></h1>
			</div>
		</div>
	</div>
</section>


<section id="sec_conselho_conselho_fiscal">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<h2><?php echo get_option('titulo_fiscal'); ?></h2>
			</div>
			<div class="col-12 text-center" id="slider_cons_fiscal">
				<?php if (isset($conselho_fiscal)) {
					global $post;
					foreach ($conselho_fiscal as $post) : $image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()));?>
						<div class="box_slide_conselho">
							<img src="<?php echo $image; ?>">
							<h3><?php the_title(); ?></h3>
							<p><?php the_field('curriculum'); ?></p>
						</div>
						<?php
					endforeach;
					wp_reset_postdata();
				}  
				function sliderConselhoFiscal(){?>
					<script type="text/javascript">
						$('#slider_cons_fiscal').slick({
							infinite: true,
							slidesToShow: 5,
							centerMode: true,
							slidesToScroll: 1,
							prevArrow: '<div class="slick-prev"><i class="fas fa-chevron-left"></i></div>',
							nextArrow: '<div class="slick-next"><i class="fas fa-chevron-right"></i></div>',
							autoplay: true,
  							autoplaySpeed: 1500,
							responsive: [
							{
								breakpoint: 1024, 
								settings: {
									infinite: true,
									slidesToShow: 3,
									slidesToScroll: 3
								}
							},
							{

								breakpoint: 600,
								settings: {
									infinite: true,
									slidesToShow: 1,
									slidesToScroll: 1
								}
							}
							]
						})
					</script>
				<?php }
				add_action('wp_footer','sliderConselhoFiscal',200);
				?>
			</div>
		</div>
	</div>	
</section>



<?php function scrollmouse(){?>
	<script type="text/javascript">
		const slider = $(".slider-item");
		slider
		.slick({
			dots: true
		});

		slider.on('wheel', (function(e) {
			e.preventDefault();

			if (e.originalEvent.deltaY < 0) {
				$(this).slick('slickNext');
			} else {
				$(this).slick('slickPrev');
			}
		}));
	</script>
<?php }
add_action('wp_footer','scrollmouse',300);

get_footer(); ?>
