<?php get_header(); ?>
<section id="sec_noticias_noticias">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-8 text-center text-md-left mb-3">
				<h2><?php single_tag_title(); ?></h2>
			</div>			
		</div>
		<div class="row my-posts">
			<?php if (have_posts()) : ?>
				<?php while (have_posts()) : the_post(); 
					$image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()));
					$_resumo = get_the_excerpt();
					$resumo = substr( $_resumo, 0, 225 );

					if ($image == ''): ?>
						<div class="col-12">
							<a href="<?php the_permalink(); ?>">
								<h3><?php the_title(); ?></h3>
								<?php if( get_field('breve_resumo') ){ ?>
								    <p><?php the_field('breve_resumo'); ?></p>
								<?php } else{ ?>							
									<p><?php echo $resumo; ?></p>	
								<?php } ?>

								<span><?php the_time( 'j \d\e  F, Y' ); ?></span>	
							</a>
							<hr>
						</div>
					<?php else: ?>
						
						<div class="col-12">
							<a href="<?php the_permalink(); ?>">
								<div class="row">
									<div class="col-12 col-md-4 box_img_noticia">
										<img src="<?php echo $image; ?>">
									</div>
									<div class="col-12 col-md-8 box_cont_noticia">
										<h3><?php the_title(); ?></h3>
										<?php if( get_field('breve_resumo') ){ ?>
										    <p><?php the_field('breve_resumo'); ?></p>
										<?php } else{ ?>							
											<p><?php echo $resumo; ?></p>	
										<?php } ?>
										<span><?php the_time( 'j \d\e  F, Y' ); ?></span>	
									</div>	
								</div>
							</a>
							<hr>
						</div>
					<?php endif; endwhile;
			        else : ?>
				<h2>Nada Encontrado</h2>
			<?php endif; 
			wp_reset_postdata();?>	
		</div>		
	</div>
</section>

<?php get_footer();

