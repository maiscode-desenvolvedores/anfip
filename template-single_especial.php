<?php
//get_header(); 
$image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()));
?>
<style type="text/css">
	/*trocar css com js*/

	.js-transparente {
		background-color: transparent !important;
		z-index: 999999;
		box-shadow: none;
		position: fixed;
		width: 100%;
	}

	.js-filtro {
		filter: brightness(100);
	}

	.js-mostrar {
		display: none;
	}

	.js-botao {
		background: transparent;
		border: 0.5px solid #FFFFFF;
	}


</style>
<section id="sec_single_especial_topo" style="background-image:linear-gradient(180deg, rgba(0, 0, 0, 0) 0%, rgba(0, 0, 0, 0.7) 100%), linear-gradient(0deg, rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('<?php echo $image; ?>');">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<h1><?php the_title(); ?></h1>
				<p><?php the_field('legenda_titulo_single'); ?></p>
				<i class="fas fa-chevron-down"></i>
			</div>
		</div>
	</div>
</section>
<section id="sec_single_especial_resumo">
	<div class="container">
		<div class="row">
			<div class="col-12 offset-md-2 col-md-8">
				<p><?php the_field('resumo_materia'); ?></p>
			</div>
		</div>
	</div>
</section>
<?php if (have_rows('sessoes')): $cont=0; ?>
	<?php while(have_rows('sessoes')): the_row(); ?>
		<section id="sec_single_especial_materia_<?php echo $cont; ?>" class="sec_single_especial_sessao" style="background-color: <?php the_sub_field('cor_sessao'); ?>">
			<div class="container">
				<div class="row">
					<div class="col-12 offset-md-2 col-md-8">

						<?php if( get_sub_field('imagem_sessao') ): ?>
							<div class="box_sessao_img" style="border-bottom: 5px solid <?php the_sub_field('cor_borda'); ?>">
								<img src="<?php the_sub_field('imagem_sessao'); ?>" style="color: <?php the_sub_field('cor_texto'); ?>">
							</div>							
						<?php endif; ?>	
						
						<h2 style="color: <?php the_sub_field('cor_texto'); ?>">
							<?php the_sub_field('titulo_sessao', false, false); ?>						
						</h2>
						<hr style="background: <?php the_sub_field('cor_borda'); ?>">
						<p style="color: <?php the_sub_field('cor_texto'); ?>">
							<?php the_sub_field('conteudo_sessao', false, false); ?>
						</p>
					</div>
				</div>
			</div>
		</section>
	<?php $cont++; 
	endwhile; ?>
<?php endif ?>

<section id="sec_single_noticia_prev_next">
	<?php 
	 wp_reset_postdata();
	$PostPrev = get_previous_post();
	$PostNext = get_next_post();

	?>
	<div class="container">
		<div class="row">
			<div class="col-12 nuvem-tags">				
				<?php if ( function_exists( 'wp_tag_cloud' ) ) : ?>				
					<p>
					<strong>Tags: </strong>
					<?php wp_tag_cloud( 'smallest=8&largest=22' ); ?>
					</p>
				<?php endif; ?>
			</div>
			<div class="col-12 col-md-9">
				<div class="row">
					<div class="col-12 col-md-6 box_prev">
						<a href="<?php the_permalink($PostPrev->ID); ?>"><i class="fas fa-angle-double-left"></i> Anterior</a>
						<?php if (get_previous_post()): ?>
							<a href="<?php the_permalink($PostPrev->ID); ?>">
								<h3><?php echo $PostPrev->post_title; ?></h3>						
							</a>

							<?php 
								$_resumo = get_the_excerpt($PostPrev->ID);
								$resumo = substr( $_resumo, 0, 225 );
	 						?>
							<?php if( get_field('breve_resumo', $PostPrev->ID) ){ ?>
							    <p><?php the_field('breve_resumo', $PostPrev->ID); ?></p>
							<?php } else{ ?>							
								<p><?php echo $resumo; ?>...</p>	
							<?php } ?>

						<?php endif ?>
					</div>
					<div class="col-12 col-md-6">
						<?php if (get_next_post()): ?>
							<div class="w-100 text-right">
								<a href="<?php the_permalink($PostNext->ID); ?>">Próximo <i class="fas fa-angle-double-right"></i></a>	
							</div>
							<a href="<?php the_permalink($PostNext->ID); ?>">
								<h3><?php echo $PostNext->post_title; ?></h3>
							</a>

							<?php 
								$_resumo = get_the_excerpt($PostNext->ID);
								$resumo = substr( $_resumo, 0, 225 );
	 						?>
							<?php if( get_field('breve_resumo', $PostNext->ID) ){ ?>
							    <p><?php the_field('breve_resumo', $PostNext->ID); ?></p>
							<?php } else{ ?>							
								<p><?php echo $resumo; ?></p>	
							<?php } ?>


						<?php endif ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php echo get_template_part('inc/template', 'form'); ?>

<?php
	function BGMenu(){ ?>

<script>

	const banner = document.getElementById('sec_single_especial_topo');

	$(document).ready(function(){
		$(window).scroll(function(){
			var scroll = $(window).scrollTop();
			if (scroll > banner.offsetHeight) {
				$(".bg_header").removeClass('js-transparente');
				$(".btn_header_menu").removeClass('js-filtro');
				$("i").removeClass('js-filtro');
				$("#sec_single_especial_topo img").removeClass('js-filtro');
				$(".box_login").removeClass('js-mostrar');
				$(".btn_associe_header").removeClass('js-botao');
			}

			else{
				$(".bg_header").addClass('js-transparente');
				$(".btn_header_menu").addClass('js-filtro');
				$("i").addClass('js-filtro');
				$("#sec_single_especial_topo img").addClass('js-filtro');
				$(".box_login").addClass('js-mostrar');
				$(".btn_associe_header").addClass('js-botao');  	
			}
		})
	})

</script>

<?php
	}
	add_action('wp_footer','BGMenu',300);?>