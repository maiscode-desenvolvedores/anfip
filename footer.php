
	<footer id="footer">
		<section id="sec_footer_conteudo">
			<div class="container">
				<div class="row box_cols">
					<div class="col-12 col-md-4 pb-5">
						<h3><?php echo get_option('titulo_anfip_footer') ?></h3>
						<p><?php echo get_option('texto_anfip_footer'); ?></p>
						<ul class="box_contato_footer">
							<li><i class="fas fa-map-marker-alt"></i><?php echo get_option('endereco'); ?></li>
							<li><i class="fas fa-phone-alt"></i><?php echo get_option('telefone'); ?></li>
							<li><i class="fas fa-envelope"></i><?php echo get_option('email'); ?></li>
						</ul>
					</div>
					<div class="col-12 col-md-4 pb-5">
						<h3>Serviços</h3>
						<div class="row">
							<div class="col-12 col-md-6">
								<?php
								$defaults = array(
									'theme_location'  => 'rodape-a',
									'container'       => false,
									'menu_class'      => 'box-rodape-a ',
									'menu_id'         => 'menu-radape-a',
									'echo'            => true
								);
								wp_nav_menu($defaults);
								?>
							</div>
							<div class="col-12 col-md-6">
								<?php
								$defaults = array(
									'theme_location'  => 'rodape-b',
									'container'       => false,
									'menu_class'      => 'box-rodape-b ',
									'menu_id'         => 'menu-radape-b',
									'echo'            => true
								);
								wp_nav_menu($defaults);
								?>
							</div>
						</div>
					</div>
					<div class="col-12 col-md-4">
						<?php dynamic_sidebar( 'custom-side-bar-footer' ); ?>
					</div>	
				</div>
			</div>
		</section>
		<section id="sec_footer_rodape">
			<div class="container-fluid">
				<div class="row">
					<div class="col-12 offset-md-3 col-md-6 text-center">
						<span><?php echo get_option('texto_rodape'); ?></span>
					</div>
					<div class="col-12 col-md-3 text-center">
						<a href="https://www.maiscode.com.br" target="_blank"> 
							<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/ico_footer.png">
						</a>
					</div>
				</div>	
			</div>
		</section>
	</footer>
	<?php wp_footer(); ?>	
</body>
</html> 