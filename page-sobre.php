<?php
/**
* Template Name: Página Sobre
*
* @package WordPress
* @author Mais Code Tecnologia
* @since First Version
*/
get_header(); 
$image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()));
?>

<section id="sec_sobre" style="background-image: url('<?php echo $image; ?>');">
	<div class="container">
		<div class="row">
			<div class="col-12 box_sobre">
				<h1><?php the_title(); ?></h1>
				<p><?php the_field('conteudo_sobre',false); ?></p>
			</div>
		</div>
	</div>
</section>



<?php get_footer(); ?>
