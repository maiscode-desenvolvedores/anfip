<section id="sec_noticias_increvase" class="sec_single_inscreverse">
	<div class="container">
		<div class="row box_inscrever_se" style="background-image: url('<?php echo get_option('imagem_form_inscrever'); ?>');">
			<div class="col-12 col-md-3">
				<span class="box_texto_inscrever_se"><?php echo get_option('texto_form_inscrever'); ?></span>
			</div>
			<div class="col-12 col-md-9 align-self-end box_form">
				<?php echo do_shortcode('[contact-form-7 id="146" title="Contact form 1"]'); ?>	
			</div>
		</div>	
	</div>
</section>