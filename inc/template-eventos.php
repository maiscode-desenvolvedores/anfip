<li class="row">
	<div class="col-12 offset-md-1 col-md-2 text-center">
		<p class="box_dia_evento"><?php the_field('dia_evento'); ?></p>
		<p class="box_mes_evento"><?php the_field('mes_evento'); ?></p>
	</div>
	<div class="col-12 col-md-9">
		<h2><?php the_title(); ?></h2>
		<p class="breve_resumo"><?php the_field('breve_resumo'); ?></p>		
	</div>
</li>
<hr class="hr_list_post">