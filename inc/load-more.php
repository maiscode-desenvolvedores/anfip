<div class="row">
    <div class="col-12 box_paginacao text-center box_vermais">
        <button class="carrega-mais btn btn_vermais" id="element">Ver mais</button>        
    </div> 
</div>

<?php 
function ajax_mais(){
    global $wp_query;
    $postType = get_post_type();
    //$page_num = get_option( 'post_per_page_' . $postType );     
    $page_num = $wp_query->post_count;
?>

    <script type="text/javascript">
        var ajaxurl = "<?php echo admin_url( 'admin-ajax.php' ); ?>";
        var controleView = false;
        var page = 1;

        jQuery(function($) {     

            $('body').on('click', '.carrega-mais', function() {
                carregaMais();
            });
            
            function carregaMais(){
                $('.loader').show();
                $('.carrega-mais').hide();

                var data = {
                    'action': 'load_posts_by_ajax',
                    'pagina': page,
                    'posts_per_page': '<?php echo $page_num; ?>',
                    'post_type': '<?php echo $postType; ?>',
                    'security': '<?php echo wp_create_nonce("load_more_posts"); ?>'
                };                
                
                setTimeout(function(){
                    $.post(ajaxurl, data, function(response) {
                        if(response != '') { 
                            $('.my-posts').append(response);
                            page++;                            
                            $('.carrega-mais').show();
                            controleView = false;
                        } else {
                            
                            $('.carrega-mais').hide();
                            controleView = "finaliza"; 
                        }   
                    });
                }, 1000);
            }
        });
    </script>
<?php  } 
add_action('wp_footer', 'ajax_mais', 500);