<?php 
$image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()));
if ($image == ''): ?>
	<div class="col-12">
		<a href="<?php the_permalink(); ?>">
			<h3><?php the_title(); ?></h3>
			<p><?php the_field('breve_resumo'); ?></p>
			<span><?php the_time('d F, Y'); ?></span>	
		</a>
		
		<hr>
	</div>
<?php else: ?>
	<div class="col-12">
		<a href="<?php the_permalink(); ?>">
		<div class="row">
			
			<div class="col-12 col-md-4 box_img_noticia">
				<img src="<?php echo $image; ?>">
			</div>
			<div class="col-12 col-md-8 box_cont_noticia">
				<h3><?php the_title(); ?></h3>
				<p><?php the_field('breve_resumo'); ?></p>
				<span><?php the_time('d F, Y'); ?></span>	
			</div>	
		
		</div>
		</a>
		<hr>
	</div>
<?php endif; ?>