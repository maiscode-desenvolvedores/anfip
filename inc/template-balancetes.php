<h2><span><?php the_title(); ?></span></h2>	
<?php if( have_rows('arquivos') ):  while ( have_rows('arquivos') ) : the_row(); ?>
<li>  
    <p><?php the_sub_field('titulo_pdf_balancete'); ?></p>
    <a href="<?php the_sub_field('pdf_balancete'); ?>" target="_blank">
        <button><?php echo get_option('texto_btn_balancetes'); ?></button>
    </a>
</li>
<hr>
<?php endwhile; endif; ?>