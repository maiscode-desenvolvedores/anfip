<?php $image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()));?>
<div class="col-12 col-md-3">
	<div class="box_img_fotos">
		<?php $checaAlbum =  get_field('album_privado', get_the_id());			
		
			if (is_bool($checaAlbum ) === true  && is_user_logged_in() ):								
		  ?>

            <a href="<?php the_permalink(); ?>">
	            <img  class="arqImgFit" src="<?php echo $image; ?>">
                <h2><?php the_title(); ?></h2>                                
                <span>Ver álbum</span>
            </a>

        <?php elseif (is_bool($checaAlbum ) === false  && !is_user_logged_in() || is_bool($checaAlbum ) === false && is_user_logged_in()) :?>

          	 <a href="<?php the_permalink(); ?>">
	            <img  class="arqImgFit" src="<?php echo $image; ?>">
                <h2><?php the_title(); ?></h2>                                
                <span>Ver álbum</span>
            </a>

         <?php else:  ?>

            <img  class="arqImgFit" src="<?php echo $image; ?>">
            <h2><?php the_title(); ?></h2>
       	    <a data-toggle="collapse" data-target="#login_restrito" aria-expanded="true"><span>Faça login para visualizar</span></a>

        <?php endif; ?>
	</div>				
</div>