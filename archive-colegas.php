<?php get_header(); ?>
<section id="sec_top_colegas" style="background-image: url(<?php echo get_option('banner_colegas'); ?>);">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<h1><?php echo get_option('titulo_colegas'); ?></h1>
			</div>
		</div>
	</div>
</section>
<section id="sec_colegas_colegas">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<!-- class="box_list_post" -->
				<table  class="table">
					<tbody class="my-posts">
						<?php if (have_posts()) : ?>
							<?php while(have_posts()) : the_post(); 
								$image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()));
								?>
								<tr>
									<th scope="row"></th>
									<td>
										<?php the_title(); ?>	
									</td>
									<td>
										<?php the_field('status_colega'); ?>	
									</td>
									<td>
										<?php the_field('cidade_colega'); ?>	
									</td>
									<td>
										<?php the_field('data_colega'); ?>	
									</td>
								</tr>
								<!-- <hr class="hr_list_post"> -->
							<?php endwhile ?>
							<?php //paginacao(); ?>
						<?php else : ?>
							<h2>Nada Encontrado</h2>
						<?php endif; ?>		
					</tbody>
				</table>
			</div>
		</div>
		<!-- Carrega Mais -->
        <?php get_template_part('inc/load', 'more'); ?>	
	</div>	
</section>
<?php get_footer(); 