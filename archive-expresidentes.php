<?php get_header(); ?>
<section id="sec_top_expresidentes" style="background-image: url(<?php echo get_option('banner_expresidentes'); ?>);">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<h1><?php echo get_option('titulo_expresidentes'); ?></h1>
			</div>
		</div>
	</div>
</section>
<section id="sec_expresidentes_expresidentes">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<!-- class="box_list_post" -->
				<table class="table">
					<thead>
						<tr>
							<th scope="col"><?php echo get_option('titulo_nomes'); ?></th>
							<th scope="col"><?php echo get_option('titulo_mandatos'); ?></th>
						</tr>
					</thead>
					<tbody class="my-posts">
						<?php if (have_posts()) : ?>
							<?php while(have_posts()) : the_post(); 
								?>
								<tr>
									<td>
										<?php the_title(); ?>	
									</td>
									<td>
										<?php the_field('periodo_mandato'); ?>	
									</td>
								</tr>
							<?php endwhile ?>
							<?php //paginacao(); ?>
						<?php else : ?>
							<h2>Nada Encontrado</h2>
						<?php endif; ?>		
					</tbody>
				</table>
			</div>
		</div>
		<!-- Carrega Mais -->
        <?php get_template_part('inc/load', 'more'); ?>
	</div>	
</section>
<?php get_footer();