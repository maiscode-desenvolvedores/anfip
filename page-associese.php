<?php 
/**
* Template Name: Página Associe-se
*
* @package WordPress
* @author Mais Code Tecnologia
* @since First Version
*/
get_header(); 

$image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()));

?>


<section id="sec_associese_topo" style="background-image: url('<?php echo $image; ?>');">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<h1><?php the_title(); ?></h1>
			</div>
		</div>
	</div>
</section>
<section id="sec_associese_form">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h2>PROPOSTA DE ATIVO, APOSENTADO E PENSIONISTA</h2>	
				<?php echo do_shortcode('[contact-form-7 id="280" title="associe-se"]'); ?>
			</div>
		</div>
	</div>
</section>


<?php get_footer(); ?>
