<?php get_header(); ?>
<?php /*
	<section id="sec_top_eventos" style="background-image: url(<?php echo get_option('banner_eventos'); ?>);">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<h1><?php echo get_option('titulo_eventos'); ?></h1>
			</div>
		</div>
	</div>
</section>
*/ ?>

<section id="sec_eventos_eventos">
	<div class="container">
		<div class="row">
            <div class="col-12">
                <h1>Eventos</h1>
            </div>
			<div class="col-12">
				<ul class="box_list_post my-posts">
				<?php if (have_posts()) : ?>
					<?php while(have_posts()) : the_post(); 
						$image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()));
						?>
						<li class="row">                            
							<div class="col-12 offset-md-1 col-md-2 text-center">
								<p class="box_dia_evento"><?php the_field('dia_evento'); ?></p>
								<p class="box_mes_evento"><?php the_field('mes_evento'); ?></p>
							</div>
							<div class="col-12 col-md-9">
								<h2><?php the_title(); ?></h2>
								<p class="breve_resumo"><?php the_field('breve_resumo'); ?></p>		
							</div>
						</li>
						<hr class="hr_list_post">
					<?php endwhile ?>
				<?php //paginacao(); ?>
				<?php else : ?>
					<h2>Nada Encontrado</h2>
				<?php endif; ?>	
				</ul>
			</div>
		</div>
		<!-- Carrega Mais -->
        <?php get_template_part('inc/load', 'more'); ?>	
	</div>	
</section>
<?php get_footer();