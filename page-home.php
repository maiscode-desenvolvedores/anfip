<?php
/**
* Template Name: Página Home
*
* @package WordPress
* @author Mais Code Tecnologia
* @since First Version
*/
get_header(); 
$args_banner = array(
  'post_type' => 'banner',

);
$all_banner = new WP_Query($args_banner);
//var_dump($all_banners);
?>
<!-- BANNER -->
<section id="carouselExampleControls" class="carousel slide sec_banner" data-ride="carousel">
  <div class="carousel-inner">
    <?php
    if($all_banner->have_posts()):
      $show = 'active';
      while ($all_banner->have_posts()) : $all_banner->the_post();
        $image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()));
        $banner_vazio = get_field('banner_vazio');
        if ($banner_vazio == true): ?>
          <div class="box_banner carousel-item justify-content-center <?php echo $show; ?>" style="background-image: url('<?php echo $image; ?>');"></div>
        <?php else: ?>
          <div class="box_banner carousel-item justify-content-center <?php echo $show; ?>" style="background-image: url('<?php echo $image; ?>');">
            <div class="container">
              <div class="row">
                <div class="col-12 col-md-7 col-lg-5 box_conteudo_banner">
                  <h2 style="background-color: <?php the_field('cor_box_titulo'); ?>; color: <?php the_field('cor_titulo'); ?>;"><?php the_title()?></h2>
                  <p><?php the_field('texto_banner'); ?></p> 
                  <button class="btn" style="background-color: <?php the_field('cor_botao'); ?>"><?php the_field('texto_btn_banner'); ?></button> 
                </div> 
              </div>
            </div>
          </div>
        <?php endif ?>
        
        <?php
        $show = '';
      endwhile;
    endif;
    ?>
  </div>
  <ol class="carousel-indicators">
      <?php
      if($all_banner->have_posts()):
        $show = 'active';
        $cont = 0;
        while ($all_banner->have_posts()) : $all_banner->the_post();
          $image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()));
          ?>
          <li data-target="#carouselExampleControls" data-slide-to="<?php echo $cont; ?>" class="<?php echo $show; ?>"></li>
          <?php
          $show = '';
          $cont++;
        endwhile;
      endif;
      ?>
    </ol> 
</section>
<?php
wp_reset_postdata();?>
<!-- ANÚNCIO -->
<section id="sec_anuncio">
  <div class="container">
    <div class="row">
      <div class="col-12 text-center">
        <?php dynamic_sidebar( 'sidebar-anuncio' ); ?>
       
      </div>
    </div>
  </div>
</section>
<!-- NOTÍCIAS -->
<?php

$args_noticia = array(
  'post_type' => 'post',
  'posts_per_page' => 4
);
$all_noticia = new WP_Query($args_noticia);

$args_post = array();
$args_post2 = array();

$Contpost = 0;
if ($all_noticia->have_posts()) : 
  while ($all_noticia->have_posts()) : $all_noticia->the_post();
    if ($Contpost < 1) {
      array_push($args_post,$post);
    }elseif($Contpost >= 1) {
      array_push($args_post2,$post);
    }
    $Contpost++;
  endwhile;
else:
  echo '<h2>Nada encontrado</h2>';
endif;
$Cont = 0;
?>
  <section id="sec_noticias">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h2><?php the_field('titulo_noticias_home',6); ?></h2>   
        </div>
        <?php 
        if (isset($args_post)) :
          $categories = get_the_category();
          global $post;
          foreach ($args_post as $post) :
            $image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID())); ?>
          
            
            <div class="col-12 col-md-8">
              <a href="<?php the_permalink(); ?>">
              <div class="box_dest_noticia" style="background-image: url('<?php echo $image; ?>');">
              <?php  
								if ( ! empty( $categories ) ) {
							?>
								 <span>  <?php echo esc_html( $categories[0]->name );   ?> </span>	   
						    <?php  
								}
							 ?>
                <h3 style="background-color: <?php the_field('cor_box_titulo'); ?>; color: <?php the_field('cor_titulo'); ?>;"><?php the_title(); ?></h3>
                <?php 
                /*
                <ul class="d-flex box_dados">
                  <li><?php the_field('data_noticia') ?></li>
                  <li><?php //the_field('viws_noticia'); ?></li>
                  <li><?php //the_field('compatilhamentos'); ?></li>
                </ul>
                */
                ?>
              </div>
              
              
              </a>
            </div>
          
          <?php
          endforeach;
        endif; ?>
        <div class="col-12 col-md-4 box_noticias_lateral">
          <?php 
          if (isset($args_post2)) :
            global $post;
            foreach ($args_post2 as $post) :?>
            <a href="<?php the_permalink(); ?>">
              <h4><?php the_title(); ?></h4>
            </a>
            <p><?php the_field('data_noticia'); ?></p>
            <hr>
            <?php 
            endforeach;
          endif;
          wp_reset_postdata();
          ?>
          <a href="<?php echo get_home_url(); ?>/publicacoes">
          <div class="box_news_lateral text-center" style="background-image: url('<?php echo get_option('banner_home'); ?>');">
              <img src="<?php echo get_option('icone_banner_home'); ?>">  
            
          </div>
          </a>
        </div>
        <div class="col-12 text-center box_vermais">
          <a href="<?php echo get_home_url(); ?>/noticias"><?php the_field('texto_vermais_noticias'); ?></a>
        </div>
      </div>
    </div>
  </section>


<!-- CONVÊNIOS E BENEFÍCIOS -->

<section id="sec_conv_benef">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h2><?php the_field('titulo_converiobeneficio'); ?></h2>  
      </div>     
      <?php 

$posts = get_field('convenios');

if( $posts ): ?>
    
    <?php foreach( $posts as $post): 
      $image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()));
      $tipo = get_field('tipo_convenio');
    ?>
        <?php setup_postdata($post); ?>
        <div class="col-12 col-md-4 text-center">
          <div class="box_conv_benef">
            <div class="box_img_conv_benef">
              <img src="<?php echo $image; ?>">  
            </div>
            <h3><?php the_title(); ?></h3>
                <p>               
              <?php               
                $_resumo = get_field('conteudo_convenio');
                $resumo = substr( $_resumo, 0, 190 ); 
                                  
              ?>    
              <?php echo $resumo; echo (strlen($resumo) >= 190) ? "..." : ""; ?></p>
          </div>
        </div>
      <?php  endforeach;  ?>
  
    <?php wp_reset_postdata(); ?>
    <div class="col-12 text-center box_vermais">
        <a href="<?php echo get_home_url(); ?>/convenio"><?php the_field('texto_vermais_conveino_beneficio'); ?></a>  
      </div>
<?php endif; ?>
    </div>
  </div>
</section>

<!-- FOTOS -->
<?php 
$args_fotos = array(
  'post_type' => 'fotos',
  'posts_per_page' => 11
);
$all_fotos = new WP_Query($args_fotos);

$col_1 = array();
$col_2 = array();
$col_3 = array();
$col_4 = array();

$cont = 1;

if ($all_fotos->have_posts()): 
  while($all_fotos->have_posts()) : $all_fotos->the_post(); 
    if ($cont <= 2) {
      array_push($col_1, $post);
    }elseif (($cont <= 5)&&($cont > 2)) {
      array_push($col_2, $post);
    }elseif (($cont <= 10)&&($cont > 5)) {
      array_push($col_3, $post);
    }else{
      array_push($col_4, $post);
    }
    $cont++;
  endwhile;
endif; 
wp_reset_postdata(); ?>
<section id="sec_fotos">
  <div class="container">
    <h2><?php the_field('titulo_fotos_home'); ?></h2>
  </div>
  <div class="container-fluid">

    <?php if (is_user_logged_in()) { ?>
 
    <div class="row">
      <div class="col-12 col-md-3">
        <div class="row">
        <?php if (isset($col_1)): 
          global $post;
          $contador = 0;
            foreach ($col_1 as $post) :
              $image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()));
              if($contador == 0):
                ?>
                <div class="col-12 box_img_home_fotos" style="height: 400px; background-image: url('');"><a href="<?php the_permalink(); ?>"><img class="arqImgFit" src="<?php echo $image ?>"></a></div>
                <?php 
                else: ?>
                  <div class="col-12 box_img_home_fotos" style="height: 200px; background-image: url('');"><a href="<?php the_permalink(); ?>"><img class="arqImgFit" src="<?php echo $image ?>"></a></div>
                  <?php 
                endif; 
                $contador++;
            endforeach;
        endif; ?>  
        </div>
      </div>
      <div class="col-12 col-md-3">
        <div class="row">
        <?php if (isset($col_2)): 
          global $post;
          $contador = 0;
            foreach ($col_2 as $post) :
              $image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()));
              
                if($contador < 2):
                  ?>
                  <div class="col-6 box_img_home_fotos" style="height: 200px; background-image: url('');"><a href="<?php the_permalink(); ?>"><img class="arqImgFit" src="<?php echo $image ?>"></a></div>
                <?php else: ?>
                  <div class="col-12 box_img_home_fotos" style="height: 400px; background-image: url('');"><a href="<?php the_permalink(); ?>"><img class="arqImgFit" src="<?php echo $image ?>"></a></div>  
                <?php endif;
              $contador++;
            endforeach;
          endif; ?> 
        </div>
      </div>
      <div class="col-12 col-md-3">
        <div class="row">
          <?php if (isset($col_3)): 
            global $post;
            $contador = 0;
            foreach ($col_3 as $post) :
              $image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()));
              if($contador != 2):
                ?>
                <div class="col-6 box_img_home_fotos" style="height: 200px; background-image: url('');"><a href="<?php the_permalink(); ?>"><img class="arqImgFit" src="<?php echo $image ?>"></a></div>
              <?php else: ?>
                <div class="col-12 box_img_home_fotos" style="height: 200px; background-image: url('');"><a href="<?php the_permalink(); ?>"><img class="arqImgFit" src="<?php echo $image ?>"></a></div>  
              <?php endif;
              $contador++;
            endforeach;
          endif; ?>
        </div>
      </div> 
      <div class="col-12 col-md-3">
        <div class="row">
          <?php if (isset($col_4)): 
            global $post;
            $contador = 0;
            foreach ($col_4 as $post) :
              $image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()));
              ?>              
                <div class="col-12 box_img_home_fotos" style="height: 600px; background-image: url('');"><a href="<?php the_permalink(); ?>"><img class="arqImgFit" src="<?php echo $image ?>"></a></div>  
              <?php
              $contador++;
            endforeach;
          endif; ?>
        </div>
      </div> 
      <div class="col-12 text-center box_vermais">
        <a href="<?php echo get_home_url(); ?>/fotos" style="line-height: 0;">ver mais >></a>  
      </div>
    </div> 
    <?php }else { ?>
      <div class="row">
      <div class="col-12 col-md-3">
        <div class="row">
        <?php if (isset($col_1)): 
          global $post;
          $contador = 0;
            foreach ($col_1 as $post) :
              $image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()));
              if($contador == 0):
                ?>
                <div class="col-12 box_img_home_fotos" style="height: 400px; background-image: url('');">
                    <img class="arqImgFit" src="<?php echo $image ?>">
                </div>
                <?php 
                else: ?>
                  <div class="col-12 box_img_home_fotos" style="height: 200px; background-image: url('');">
                      <img class="arqImgFit" src="<?php echo $image ?>">
                  </div>
                  <?php 
                endif; 
                $contador++;
            endforeach;
        endif; ?>  
        </div>
      </div>
      <div class="col-12 col-md-3">
        <div class="row">
        <?php if (isset($col_2)): 
          global $post;
          $contador = 0;
            foreach ($col_2 as $post) :
              $image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()));
              
                if($contador < 2):
                  ?>
                  <div class="col-6 box_img_home_fotos" style="height: 200px; background-image: url('');">
                      <img class="arqImgFit" src="<?php echo $image ?>">
                  </div>
                <?php else: ?>
                  <div class="col-12 box_img_home_fotos" style="height: 400px; background-image: url('');">
                      <img class="arqImgFit" src="<?php echo $image ?>">
                  </div>  
                <?php endif;
              $contador++;
            endforeach;
          endif; ?> 
        </div>
      </div>
      <div class="col-12 col-md-3">
        <div class="row">
          <?php if (isset($col_3)): 
            global $post;
            $contador = 0;
            foreach ($col_3 as $post) :
              $image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()));
              if($contador != 2):
                ?>
                <div class="col-6 box_img_home_fotos" style="height: 200px; background-image: url('');">
                    <img class="arqImgFit" src="<?php echo $image ?>">
                </div>
              <?php else: ?>
                <div class="col-12 box_img_home_fotos" style="height: 200px; background-image: url('');">
                    <img class="arqImgFit" src="<?php echo $image ?>">
                </div>  
              <?php endif;
              $contador++;
            endforeach;
          endif; ?>
        </div>
      </div> 
      <div class="col-12 col-md-3">
        <div class="row">
          <?php if (isset($col_4)): 
            global $post;
            $contador = 0;
            foreach ($col_4 as $post) :
              $image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()));
              ?>              
                <div class="col-12 box_img_home_fotos" style="height: 600px; background-image: url('');">
                    <img class="arqImgFit" src="<?php echo $image ?>">
                </div>  
              <?php
              $contador++;
            endforeach;
          endif; ?>
        </div>
      </div> 
      <div class="col-12 text-center box_vermais">
        <a href="<?php echo get_home_url(); ?>/fotos" style="line-height: 0;">ver mais >></a>  
      </div>
    </div> 
    <?php } ?>
  </div>
</section>

<?php get_footer(); ?>